<%@ LANGUAGE=VBScript %>
<html>
<head>
<title>Rover For HomeSeer</title>

<%
' Rover: simple, small controls for HomeSeer
' (c) 2003-2011 by Frank J. Perricone, hawthorn@foobox.com
' see rover\manual.txt for installation, customization, and other docs

' *****************************************************************************
dim i, j, k, l, dev, s, qaction, qshow, unitcode
dim locations(64)
dim numlocations
dim opt_style, opt_showhidden, opt_controls, opt_refresh, opt_composescenes, opt_devicecutoff, opt_eventcutoff, opt_loglines, opt_gridcolumns, opt_hidedim, opt_menus, opt_framelinks, opt_equalcolumns, opt_navattop
dim opt_iconsdir, opt_snippetsdir
dim tricks_installed
dim rover_version
dim trick_inifile

rover_version = "3.0"

trick_inifile = "rover_tricks.ini"
tricks_installed = 0
if FileExists(hs.GetAppPath & "\html\rover\tricks\rover_tricks.asp") then
    tricks_installed = 1
  end if

qaction = request.querystring("action")
if qaction = "" then qaction = "none"
qshow = request.querystring("show")
if qshow = "" then qshow = "list"

' start by loading defaults, but everything else will override them
s = hs.GetINISetting("Settings","Defaults","","rover.ini")
if s <> "" then ' &options=####&icons=###&snippets=####
  opt_iconsdir = hs.StringItem(s,3,"&")
  if opt_iconsdir <> "" then opt_iconsdir = mid(opt_iconsdir,6)
  opt_snippetsdir = hs.StringItem(s,4,"&")
  if opt_snippetsdir <> "" then opt_snippetsdir = mid(opt_snippetsdir,9)
  ParseOptions mid(hs.StringItem(s,2,"&"),9)
end if
' parse out the folder locations
if request.querystring("icons") <> "" then opt_iconsdir = request.querystring("icons")
if opt_iconsdir = "" then opt_iconsdir = "icons"
if request.querystring("snippets") <> "" then opt_snippetsdir = request.querystring("snippets")
if opt_snippetsdir = "" then opt_snippetsdir = "snippets"
' parse out the options from the main field
if request.querystring("options") <> "" then ParseOptions request.querystring("options")
' now check for backwards compatibility overrides
if request.querystring("style") <> "" then opt_style = lcase(request.querystring("style"))
if request.querystring("showhidden") <> "" then opt_showhidden = lcase(request.querystring("showhidden"))
if request.querystring("controls") <> "" then opt_controls = lcase(request.querystring("controls"))
if request.querystring("refresh") <> "" then opt_refresh = request.querystring("refresh")
if request.querystring("composescenes") <> "" then opt_composescenes = request.querystring("composescenes")
if request.querystring("devicecutoff") <> "" then opt_devicecutoff = request.querystring("devicecutoff")
if request.querystring("eventcutoff") <> "" then opt_eventcutoff = request.querystring("eventcutoff")
if request.querystring("loglines") <> "" then opt_loglines = request.querystring("loglines")
if request.querystring("gridcolumns") <> "" then opt_gridcolumns = request.querystring("gridcolumns")
if request.querystring("hidedim") <> "" then opt_hidedim = request.querystring("hidedim")
if request.querystring("menus") <> "" then opt_menus = request.querystring("menus")
if request.querystring("framelinks") <> "" then opt_menus = request.querystring("framelinks")
if request.querystring("equalcolumns") <> "" then opt_equalcolumns = request.querystring("equalcolumns")
if request.querystring("navattop") <> "" then opt_navattop = request.querystring("navattop")
' now normalize the options
if opt_style <> "text" and opt_style <> "grid" then opt_style = "table"
if opt_showhidden <> "yes" then opt_showhidden = "no"
if opt_controls <> "text" then opt_controls = "icons"
if not IsNumeric(opt_refresh) then opt_refresh = 0 else opt_refresh = cint(opt_refresh)
if opt_refresh < 0 then opt_refresh = 0
if not IsNumeric(opt_composescenes) then opt_composescenes = 4 else opt_composescenes = cint(opt_composescenes)
if opt_composescenes < 1 then opt_composescenes = 1
if opt_composescenes > 12 then opt_composescenes = 12
if not IsNumeric(opt_devicecutoff) then opt_devicecutoff = 0 else opt_devicecutoff = cint(opt_devicecutoff)
if opt_devicecutoff < 10 and opt_devicecutoff <> 0 then opt_devicecutoff = 10
if opt_devicecutoff > 99 then opt_devicecutoff = 0
if not IsNumeric(opt_eventcutoff) then opt_eventcutoff = 0 else opt_eventcutoff = cint(opt_eventcutoff)
if opt_eventcutoff < 10 and opt_eventcutoff <> 0 then opt_eventcutoff = 10
if opt_eventcutoff > 99 then opt_eventcutoff = 0
if not IsNumeric(opt_loglines) then opt_loglines = 0 else opt_loglines = cint(opt_loglines)
if opt_loglines < 0 then opt_loglines = 0
if opt_loglines > 999 then opt_loglines = 0
if not IsNumeric(opt_gridcolumns) then opt_gridcolumns = 4 else opt_gridcolumns = cint(opt_gridcolumns)
if opt_gridcolumns < 2 then opt_gridcolumns = 2
if opt_gridcolumns > 9 then opt_gridcolumns = 9
if opt_hidedim <> "yes" then opt_hidedim = "no"
if opt_menus <> "yes" then opt_menus = "no"
if opt_framelinks <> "yes" then opt_framelinks = "no"
if opt_equalcolumns <> "yes" then opt_equalcolumns = "no"
if opt_navattop <> "yes" then opt_navattop = "no"

' add a refresh tag if the URL specifies one is desired
if opt_refresh <> 0 and left(qshow,4) <> "link" then
	  response.write "<meta HTTP-EQUIV='refresh' CONTENT='" & opt_refresh
    response.write ";URL=/rover.asp?show="
    if qshow = "clearlog" then response.write "log" else response.write qshow
    response.write ReplicateSettings() & "'>"
  end if ' this idea borrowed from Pete Garyga's ce.asp

' build the page header
response.write "<style type='text/css'>"
response.write "select {height: 36px; max-height: 36px; font-size: 24px}"
response.write "a {text-decoration:none}"
response.write "</style>"

if tricks_installed and left(qshow,5) = "trick" and len(qshow)>5 and TrickHasAClock(Replace(mid(qshow,6),"_"," ")) then
  response.write "<script type='text/javascript'>" & vbCrLf
  response.write "<!--" & vbCrLf
  response.write "function updateClock ( ) {" & vbCrLf
  response.write "var currentTime = new Date ( );" & vbCrLf
  response.write "var currentHours = currentTime.getHours ( );" & vbCrLf
  response.write "var currentMinutes = currentTime.getMinutes ( );" & vbCrLf
  response.write "var currentSeconds = currentTime.getSeconds ( );" & vbCrLf
  response.write "currentMinutes = ( currentMinutes < 10 ? " & chr(34) & "0" & chr(34) & " : " & chr(34) & chr(34) & " ) + currentMinutes;" & vbCrLf
  response.write "currentSeconds = ( currentSeconds < 10 ? " & chr(34) & "0" & chr(34) & " : " & chr(34) & chr(34) & " ) + currentSeconds;" & vbCrLf
  response.write "var timeOfDay = ( currentHours < 12 ) ? " & chr(34) & "a" & chr(34) & " : " & chr(34) & "p" & chr(34) & ";" & vbCrLf
  response.write "currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;" & vbCrLf
  response.write "currentHours = ( currentHours == 0 ) ? 12 : currentHours;" & vbCrLf
  response.write "var currentTimeString = currentHours + " & chr(34) & ":" & chr(34) & " + currentMinutes + " & chr(34) & ":" & chr(34) & " + currentSeconds + timeOfDay;" & vbCrLf
  response.write "document.getElementById(" & chr(34) & "roverclock" & chr(34) & ").firstChild.nodeValue = currentTimeString;" & vbCrLf
  response.write "document.getElementById(" & chr(34) & "clock" & chr(34) & ").firstChild.nodeValue = currentTimeString;" & vbCrLf
  response.write "}" & vbCrLf
  response.write "// -->" & vbCrLf
  response.write "</script>" & vbCrLf
end if

WriteSnippet "head.html"
response.write "</head><body"
WriteSnippet "bodytag.html"
response.write ">"
WriteSnippet "bodytop.html"

' build a list of locations
BuildLocationsList

' handle actions, checking for authorizations if needed
if qaction <> "none" then
    if GetUserAuthorizations(hs.WebLoggedInUser) <= 1 then
		    response.write "<p><b><i>Sorry, control is available to authorized users only!</i></b></p>"
        hs.WriteLog "Rover","Ignoring action from " & hs.WebLoggedInUser & " at " & request.ServerVariables("REMOTE_ADDR")
		  else
        HandleAction qaction, qshow
      end if
  end if

WriteSnippet "afteraction.html"

if opt_navattop = "yes" then 
  BottomLinks qshow
  WriteSnippet "bodybottom.html"
end if

' handle shows
if qshow = "about" then
    ShowAbout
  elseif qshow = "options" then
    ShowOptions
  elseif qshow = "log" then
    ShowLog 0
  elseif qshow = "fulllog" then
    ShowLog 3
  elseif qshow = "clearlog" then
    ShowLog 1
  elseif qshow = "switchlog" then
    ShowLog 2
  elseif qshow = "list" then
    ShowList
  elseif left(qshow,5) = "event" then
    ShowEvents Replace(Mid(qshow,6),"_"," ")
  elseif left(qshow,3) = "loc" and len(qshow)>3 then
    ShowLocation mid(qshow,4)
  elseif left(qshow,4) = "link" and len(qshow)>4 then
    ShowLink DecodeString(mid(qshow,5))
  elseif left(qshow,5) = "trick" and len(qshow)>5 and tricks_installed then
    ShowTrick Replace(mid(qshow,6),"_"," ")
  elseif left(qshow,3) = "dev" and len(qshow)>3 then
    ShowDev mid(qshow,4)
  else
    ShowLocation qshow
    qshow = "loc" & qshow
  end if

WriteSnippet "aftershow.html"

' handle special content in the file for this show
if opt_style = "grid" then
  WriteSnippet "grid-" & replace(replace(lcase(qshow)," ",""),"_","") & ".html"
else
  WriteSnippet replace(replace(lcase(qshow)," ",""),"_","") & ".html"
end if

WriteSnippet "afterspecial.html"

' include links to other shows, log, about
if opt_navattop = "no" then 
  BottomLinks qshow
  WriteSnippet "bodybottom.html"
end if
response.write "</body></html>"

' end of main routine -- everything else is functions and subs



' *****************************************************************************
sub ParseOptions(optstring)
  ' if the string is present, its characters are as follows (and can cut off early):
  ' style(1): 0=text, 1=table, 2=grid
  if optstring = "" then exit sub
  s = PeelCharsFromOptionString(optstring,1)
  if s = "1" then opt_style = "table" else if s = "2" then opt_style = "grid" else opt_style = "text"
  ' showhidden(1): 0=no, 1=yes
  if optstring = "" then exit sub
  if PeelCharsFromOptionString(optstring,1) = "1" then opt_showhidden = "yes" else opt_showhidden = "no"
  ' controls(1): 0=text, 1=icons
  if optstring = "" then exit sub
  if PeelCharsFromOptionString(optstring,1) = "1" then opt_controls = "icons" else opt_controls = "text"
  ' refresh(3): number of seconds, 0 for none
  if optstring = "" then exit sub
  opt_refresh = PeelCharsFromOptionString(optstring,3)
  ' composescenes(1): number of scenes, in hex
  if optstring = "" then exit sub
  opt_composescenes = PeelCharsFromOptionString(optstring,1)
  if opt_composescenes >= "A" then opt_composescenes = cstr(asc(opt_composescenes) - asc("A") + 11)
  if opt_composescenes >= "a" then opt_composescenes = cstr(asc(opt_composescenes) - asc("a") + 11)
  ' devicecutoff(2): number of characters of device names to show
  if optstring = "" then exit sub
  opt_devicecutoff = PeelCharsFromOptionString(optstring,2)
  ' eventcutoff(2): number of characters of event names to show
  if optstring = "" then exit sub
  opt_eventcutoff = PeelCharsFromOptionString(optstring,2)
  ' loglines(3): number of lines of log to show by default
  if optstring = "" then exit sub
  opt_loglines = PeelCharsFromOptionString(optstring,3)
  ' opt_gridcolumns(1): number of columns when in grid format
  if optstring = "" then exit sub
  opt_gridcolumns = PeelCharsFromOptionString(optstring,1)
  ' hidedim(1): 0=no, 1=yes
  if optstring = "" then exit sub
  if PeelCharsFromOptionString(optstring,1) = "1" then opt_hidedim = "yes" else opt_hidedim = "no"
  ' menus(1): 0=no, 1=yes
  if optstring = "" then exit sub
  if PeelCharsFromOptionString(optstring,1) = "1" then opt_menus = "yes" else opt_menus = "no"
  ' framelinks(1): 0=no, 1=yes
  if optstring = "" then exit sub
  if PeelCharsFromOptionString(optstring,1) = "1" then opt_framelinks = "yes" else opt_framelinks = "no"
  ' opt_equalcolumns(1): 0=no, 1=yes
  if optstring = "" then exit sub
  if PeelCharsFromOptionString(optstring,1) = "1" then opt_equalcolumns = "yes" else opt_equalcolumns = "no"
  ' opt_navattop(1): 0=no, 1=yes
  if optstring = "" then exit sub
  if PeelCharsFromOptionString(optstring,1) = "1" then opt_navattop = "yes" else opt_navattop = "no"
end sub 

' *****************************************************************************
Function PeelCharsFromOptionString(ByRef optstring, numchars)
  PeelCharsFromOptionString = Left(optstring, numchars)
  optstring = Mid(optstring, numchars+1)
End function

' *****************************************************************************
sub BuildLocationsList()
  ' Build a list of locations for later use (up to 64 locations)
  dim dev, devices, thisloc
  numlocations = 0

  ' loop through devices
  set devices = hs.GetDeviceEnumerator
  if IsObject(devices) then
      do while not devices.Finished
        set dev = devices.GetNext
        if not dev is nothing then
            if opt_showhidden = "yes" or (dev.misc and &H20) = 0 then
                l = 1 ' yes, add it
                thisloc = dev.location
                if thisloc = "" then thisloc = "No Location"
                ' see if this location is already listed
                for k = 1 to numlocations
                  if lcase(thisloc) = lcase(locations(k)) then
                      l = 0 ' don't bother, it's already there
                      exit for
                    end if
                   next
                ' if we're due to add it to the list,
                if l = 1 and numlocations < 64 then ' add it
                    numlocations = numlocations + 1
                    locations(numlocations) = thisloc
                  end if
              end if
          end if
        loop
    end if

  ' sort the list -- simple selection sort
  if numlocations > 2 then ' sort the list
      for i = 1 to numlocations-1
        k = i
        for j = i+1 to numlocations
          if locations(j) < locations(k) then k = j
          next
        if k <> i then
            s = locations(k)
            locations(k) = locations(i)
            locations(i) = s
          end if
        next
    end if
end sub


' *****************************************************************************
sub HandleAction(qaction, qshow)
  ' execute an action; we're assuming that guests have already been filtered out
  s = lcase(qaction)

  ' figure out which action it is
  if left(s,3) = "dev" then ' dev<unitcode><cmd> where <cmd> can be on, off, abs<val>, dim<val>, bright<val>
      HandleDeviceAction mid(qaction,4),qshow

    elseif left(s,7) = "compose" then ' Compose device
      HandleComposeAction mid(qaction,8),qshow

    elseif left(s,5) = "therm" then ' therm<unitcode><cmd> where <cmd> can be heat, cool, auto, off, or set<newsetpoint>
      HandleThermostatAction mid(qaction,6),qshow

    elseif left(s,5) = "event" then ' event<name>
      hs.TriggerEvent Replace(mid(qaction,6),"_"," ")
      response.write "<p>Event <b>" & Replace(mid(qaction,6),"_"," ") & "</b> triggered.</p>"
      hs.WriteLog "Rover","Triggering event " & Replace(mid(qaction,6),"_"," ") & " from " & hs.WebLoggedInUser & " at " & request.ServerVariables("REMOTE_ADDR")

    elseif left(s,2) = "ir" then ' ir<string>
      hs.SendIR Replace(mid(qaction,3),"_"," ")
      response.write "<p>Sent infrared command <b>" & Replace(mid(qaction,3),"_"," ") & "</b>.</p>"
      hs.WriteLog "Rover","Sending infrared " & Replace(mid(qaction,3),"_"," ") & " from " & hs.WebLoggedInUser & " at " & request.ServerVariables("REMOTE_ADDR")

    elseif left(s,3) = "cmd" then ' cmd<cmd>
      Execute Replace(mid(qaction,4),"_"," ")
      response.write "<p>Executed command <b>" & Replace(mid(qaction,4),"_"," ") & "</b>.</p>"
      hs.WriteLog "Rover","Executing command " & Replace(mid(qaction,4),"_"," ") & " from " & hs.WebLoggedInUser & " at " & request.ServerVariables("REMOTE_ADDR")

    else ' unknown action
      response.write "<p>Unknown action <b>" & qaction & "</b> ignored.</p>"
      hs.WriteLog "Rover","Ignoring unknown action " & qaction & " from " & hs.WebLoggedInUser & " at " & request.ServerVariables("REMOTE_ADDR")
    end if
end sub


' *****************************************************************************
sub HandleDeviceAction(qaction, qshow)
  ' execute a device action
  ' string is <unitcode><cmd> where <cmd> can be on, off, abs<val>, dim<val>, bright<val>, or button<number>
  dim cmd

  ' parse out the unitcode and the command
  i = ParseUnitcode(qaction)
  unitcode = left(qaction,i-1)
  cmd = lcase(mid(qaction,i))

  ' find the device validating if it exists
  if hs.DeviceExists(unitcode) = -1 then
      response.write "<p>No device exists at unitcode <b>" & unitcode & "</b>!</p>"
      exit sub
    end if
  set dev = hs.GetDeviceByRef(hs.DeviceExistsRef(unitcode))
  
  ' figure out what the command is
  if left(cmd,2) = "on" then
      cmd = "Turned on"
      if dev.can_dim then
          hs.ExecX10 unitcode,"ddim",100
        else ' just turn it on
          hs.ExecX10 unitcode,"on"
        end if

    elseif left(cmd,3) = "off" then
      cmd = "Turned off"
      hs.ExecX10 unitcode,"off"

    elseif left(cmd,3) = "abs" then ' absolute dim level
      ' what's the current and target dim level?
      l = hs.DeviceValue(unitcode)
      k = cint(mid(cmd,4))
      if k < 0 or k > 100 then
          response.write "<p>Invalid dim level <b>" & k & "</b> ignored.</p>"
          exit sub
        end if
      cmd = "Dimmed to " & cstr(k) & "%"
      if dev.can_dim then
          hs.ExecX10 unitcode,"ddim",k
        else ' just turn it on
          response.write "<p>Attempted to dim a non-dimmable device <b>" & dev.name & "</b> ignored.</p>"
        end if

    ' relative dim and bright commands are not generated by rover.asp but the user could put them into the plug-in files
    elseif left(cmd,3) = "dim" then ' relative dim
      ' figure out the current and target dim level
      l = hs.DeviceValue(unitcode)
      k = cint(mid(cmd,4))
      if k <= 0 or k > l then
          response.write "<p>Invalid relative dim level <b>" & k & "</b> ignored.</p>"
          exit sub
        end if
      cmd = "Dimmed by " & cstr(k) & "%"
      if dev.can_dim then
          hs.ExecX10 unitcode,"ddim",l-k
        else ' just turn it on
          response.write "<p>Attempted to dim a non-dimmable device <b>" & dev.name & "</b> ignored.</p>"
        end if

    elseif left(cmd,6) = "bright" then ' relative bright
      ' figure out the current and target dim level
      l = hs.DeviceValue(unitcode)
      k = cint(mid(cmd,7))
      if k <= 0 or k > (100-l) then
          response.write "<p>Invalid relative bright level <b>" & k & "</b> ignored.</p>"
          exit sub
        end if
      cmd = "Brightened by " & cstr(k) & "%"
      if dev.can_dim then
          hs.ExecX10 unitcode,"ddim",l+k
        else ' just turn it on
          response.write "<p>Attempted to dim a non-dimmable device <b>" & dev.name & "</b> ignored.</p>"
        end If
        
    elseif left(cmd,6) = "button" then ' custom button
      k = cint(mid(cmd,7))
      l = hs.StringItem(dev.buttons,k,chr(1))
      cmd = "Executed the " & hs.StringItem(l,2,chr(2)) & " function"
      if l = "" Then
          response.write "<p>Undefined custom button " & k & " on <b>" & dev.name & "</b> ignored.</p>"
        else
          'network_monitor.txt("check_unitcode","N1")[1]Heat
          k = hs.StringItem(l,2,"(") ' "check_unitcode","N1")
          k = hs.StringItem(k,1,")") ' "check_unitcode","N1"
          k = replace(k,"""","")     ' check_unitcode,N1
          'hs.WriteLog "TRACE","Rover doing a plugin button: hs.RunEx(" & hs.StringItem(l,1,"(") & "," & hs.StringItem(k,1,",") & "," & hs.StringItem(k,2,",") & ")"
          l = hs.RunEx(hs.StringItem(l,1,"("),hs.StringItem(k,1,","),hs.StringItem(k,2,","))
          if cstr(l) <> "" then cmd = cmd & " (" & cstr(l) & ")"
          cmd = cmd & " for"
        end if

    else ' unknown action
      response.write "<p>Unknown device action <b>" & qaction & "</b> ignored.</p>"
      exit sub
    end if

  ' report the outcome to the user and log it
  response.write "<p>" & cmd & " the <b>" & dev.name & "</b>.</p>"
  hs.WriteLog "Rover","Device action: " & cmd & " the " & dev.location & " " & dev.name & " at " & unitcode & " from " & hs.WebLoggedInUser & " at " & request.ServerVariables("REMOTE_ADDR")
end sub


' *****************************************************************************
sub HandleComposeAction(qaction, qshow)
  ' execute a Compose scene action
  ' string is <unitcode><cmd> where <cmd> is off, on, or scene<num>
  dim cmd, r

  ' parse out the unitcode and the command
  i = ParseUnitcode(qaction)
  unitcode = left(qaction,i-1)
  cmd = lcase(mid(qaction,i))

  ' find the device validating if it exists
  if hs.DeviceExists(unitcode) = -1 then
      response.write "<p>No device exists at unitcode <b>" & unitcode & "</b>!</p>"
      exit sub
    end if
  set dev = hs.GetDeviceByRef(hs.DeviceExistsRef(unitcode))
  
  if left(cmd,2) = "on" then ' Scene On
      cmd = "Turned on"
      r = hs.plugin("Compose Scenes").SendCmd(left(unitcode,1), mid(unitcode,2), "Scene On")

    elseif left(cmd,3) = "off" then ' Scene Off
      cmd = "Turned off"
      r = hs.plugin("Compose Scenes").SendCmd(left(unitcode,1), mid(unitcode,2), "Scene Off")

    elseif left(cmd,5) = "scene" then ' scene

      cmd = mid(cmd,6) ' strip the word "scene"
      ' validate the scene
      if not isnumeric(cmd) then
          response.write "<p>Invalid Compose scene <b>" & scene & "</b> ignored.</p>"
          exit sub
        end if
      ' execute the scene
      r = hs.plugin("Compose Scenes").SetScene(left(unitcode,1), mid(unitcode,2), cint(cmd))
      cmd = "Executed scene <i>" & hs.GetINISetting("Compose","Scene-" & left(unitcode,1) & "-" & mid(unitcode,2) & "-" & cmd,"Scene " & chr(asc("A") + cmd - 1)) & " on"

    else ' unknown action
      response.write "<p>Unknown device action <b>" & qaction & "</b> ignored.</p>"
      exit sub
    end if

  ' report the outcome to the user and log it
  response.write "<p>" & cmd & " the <b>" & dev.name & "</b>.</p>"
  hs.WriteLog "Rover","Device action: " & cmd & " the " & dev.location & " " & dev.name & " at " & unitcode & " from " & hs.WebLoggedInUser & " at " & request.ServerVariables("REMOTE_ADDR")
end sub


' *****************************************************************************
sub HandleThermostatAction(qaction, qshow) ' execute a thermostat action
  ' string is <unitcode><cmd> where <cmd> can be heat, cool, auto, off, or set<newsetpoint>
  dim cmd

  ' parse out the unitcode and the command
  i = ParseUnitcode(qaction)
  unitcode = left(qaction,i-1)
  cmd = lcase(mid(qaction,i))

  ' find the device validating if it exists
  if hs.DeviceExists(unitcode) = -1 then
      response.write "<p>No device exists at unitcode <b>" & unitcode & "</b>!</p>"
      exit sub
    end if
  set dev = hs.GetDeviceByRef(hs.DeviceExistsRef(unitcode))
  
  ' figure out what the command is
  if left(cmd,4) = "heat" then
      hs.ControlThermostat unitcode, "SetMode", "1"
      cmd = "Set thermostat to heat mode"
    elseif left(cmd,4) = "cool" then
      hs.ControlThermostat unitcode, "SetMode", "2"
      cmd = "Set thermostat to cool mode"
    elseif left(cmd,4) = "auto" then
      hs.ControlThermostat unitcode, "SetMode", "3"
      cmd = "Set thermostat to auto mode"
    elseif left(cmd,3) = "off" then
      hs.ControlThermostat unitcode, "SetMode", "0"
      cmd = "Set thermostat off"
    elseif left(cmd,3) = "set" then
      k = cint(mid(cmd,4))
      if k < 50 or k > 120 then
          response.write "<p>Invalid thermostat setpoint <b>" & k & "</b> ignored.</p>"
          exit sub
        end if
      hs.ControlThermostat unitcode, "SetSetPoint", cstr(k)
      cmd = "Set thermostat setpoint to " & cstr(k)
    else ' unknown action
      response.write "<p>Unknown thermostat action <b>" & qaction & "</b> ignored.</p>"
    end if

  ' report the outcome to the user and log it
  response.write "<p>" & cmd & " on the <b>" & dev.location & " " & dev.name & "</b>.</p>"
  hs.WriteLog "Rover","Thermostat action: " & cmd & " on the " & dev.location & " " & dev.name & " at " & unitcode & " from " & hs.WebLoggedInUser & " at " & request.ServerVariables("REMOTE_ADDR")
end sub


' *****************************************************************************
function ParseUnitcode(qaction)
  ' find the break-point between the unitcode and its following command
  ' the unitcode is always a single character followed by one or more digits
  ' the following command always starts with a non-digit
  ' so we can look for digits
  dim r
  r = 2 ' skip over the housecode
  while r <= len(qaction) and mid(qaction,r,1) >= "0" and mid(qaction,r,1) <= "9"
    r = r + 1
    wend
  ParseUnitcode = r
end function


' *****************************************************************************
sub ShowAbout()
  ' show an about page
  response.write "<h3>Rover v" & rover_version & " for HomeSeer v" & hs.Version & "</h3>"
	response.write "<p>Copyright (c) 2003-2011 by <a href='http://www.foobox.com/~hawthorn/automate.html'>Frank J. Perricone</a><br>"
	response.write "(Hunter Green on the <a href='http://forums.homeseer.com/'>HomeSeer webboard</a>)<br>"
	response.write "Email: <a href='mailto:hawthorn@foobox.com'>hawthorn@foobox.com</a></p>"
	response.write "<p>Support, news and updates for Rover are available at the <a href='http://rover-for-homeseer.blogspot.com/2006/06/welcome.html'>Rover for HomeSeer blog</a>.</p>"
	response.write "<p>Rover is designed to be the simplest possible web interface to HomeSeer that still gives full control over your house.&nbsp; The intention is, you can use it with a handheld of any sort, no matter what the browser or screen size; a text-only browser; a slow dial-up line; etc.&nbsp; It doesn't require browser support for forms, tables, even images, so it'll run on anything, fast.&nbsp; But it'll still control all your devices, even thermostats, and let you run all your events.</p>"
  response.write "<p><a href='rover/manual.html'>Read the Rover User Manual</a></p>"
  response.write "Thanks to TechnoCat for the <a href='http://forums.homeseer.com/showthread.php?p=781660#post781660'>devstat snipped substitution</a> feature.</p>"
end sub


' *****************************************************************************
sub ShowOptions()
  ' save defauts if so requested
  if request.querystring("setdefaults") = "yes" then
    hs.SaveINISetting "Settings","Defaults",ReplicateSettings(),"rover.ini"
    response.write "<h2>Settings have been saved as defaults.</h2>"
  end if
  ' show an options page
  response.write "<h3>Rover Options</h3>"
  ShowOption "Style",opt_style,"table","text","grid","","","","Determines the style of display for device lists"
  ShowOption "Menus",opt_menus,"yes","no","","","","","Use pull-down menus for the bottom bar"
  ShowOption "NavAtTop",opt_navattop,"yes","no","","","","","Move navigation links/menus to the top"
  ShowOption "ShowHidden",opt_showhidden,"yes","no","","","","","Determines if hidden devices are displayed"
  ShowOption "Controls",opt_controls,"icons","text","","","","","Show controls as icons or text links"
  ShowOption "HideDim",opt_hidedim,"yes","no","","","","","Hide dim controls (show only on/off)"
  ShowOption "Refresh",opt_refresh,"none","30","60","90","120","240","Number of seconds between automatic page refreshes"
  ShowOption "GridColumns",opt_gridcolumns,"2","3","4","5","6","8","How many columns in grid view"
  ShowOption "EqualColumns",opt_equalcolumns,"yes","no","","","","","Force grid columns to equal sizes"
  ShowOption "ComposeScenes",opt_composescenes,"0","2","4","6","8","12","Number of scenes to show on Compose devices"
  ShowOption "DeviceCutoff",opt_devicecutoff,"none","10","20","30","40","50","Trim device names in lists to this many characters long"
  ShowOption "EventCutoff",opt_eventcutoff,"none","10","20","30","40","50","Trim event names in lists to this many characters long"
  ShowOption "LogLines",opt_loglines,"10","25","50","100","200","full","How many lines of the log to show"
  if tricks_installed then ShowOption "FrameLinks",opt_framelinks,"yes","no","","","","","Go to trick links in a frame"
  ShowOptionFolders "Icons","gif,jpg,bmp,png",opt_iconsdir,"Select a folder of icons to use"
  ShowOptionFolders "Snippets","html",opt_snippetsdir,"Select a folder of snippets to use"
  response.write "<p>After selecting your options, browse to the location desired and make your bookmark, then use that bookmark in future to apply those settings.</p>"
  response.write "<p>You can also <a href='rover.asp?show=options&setdefaults=yes" & ReplicateSettings() & "'>set these settings as defaults</a>.&nbsp; Note that bookmark-stored settings will always take precedence.</p>"
end sub


' *****************************************************************************
sub ShowOptionFolders(name, filetypes, actualval, explanation)
  dim v(5), i, foundone, filetype
  dim fso, fldr, subfolders, subfolder, files, file
  i = 1
  v(1) = ""
  v(2) = ""
  v(3) = ""
  v(4) = ""
  v(5) = ""
  set fso = CreateObject("Scripting.FileSystemObject")
  set fldr = fso.GetFolder(hs.GetAppPath & "\html\rover\")
  set subfolders = fldr.SubFolders
  for each subfolder in subfolders
    if i <= 5 and lcase(subfolder.Name) <> lcase(name) and lcase(subfolder.Name) <> "grid" and lcase(subfolder.Name) <> "tricks" Then
      ' verify if it has at least one of the right kind of file in it
      set files = subfolder.Files
      foundone = 0
      for each file in files
        filetype = right(file.Name, len(file.Name) - instrRev(file.Name,"."))
        if InStr("," & filetypes & ",","," & filetype & ",") <> 0 then foundone = 1
        next
      if foundone = 1 then
          v(i) = subfolder.Name
          i = i + 1
        end if
      end if
    next
  ShowOption name, actualval, lcase(name), v(1), v(2), v(3), v(4), v(5), explanation
end sub


' *****************************************************************************
sub ShowOption(name, actualval, val1, val2, val3, val4, val5, val6, explanation)
  ' show a single option
	response.write "<p><b>" & name & "</b>: "
  ShowOptionValue name,actualval,val1
  ShowOptionValue name,actualval,val2
  ShowOptionValue name,actualval,val3
  ShowOptionValue name,actualval,val4
  ShowOptionValue name,actualval,val5
  ShowOptionValue name,actualval,val6
  response.write "<br>&nbsp; <small>" & explanation & "</small></p>"
end sub


' *****************************************************************************
function ReplicateFolderSetting(fieldname, value)
  If lcase(fieldname) <> lcase(value) then ReplicateFolderSetting = "&" & lcase(fieldname) & "=" & value else ReplicateFolderSetting = ""
end function


' *****************************************************************************
sub ShowOptionValue(name, actualval, value)
  dim s
  if value = "" then exit sub
  response.write "<a href='rover.asp?show=options"
  ' develop an options string that includes current settings and one override
  if name = "Style" then s = ReplicateTupleSetting(value,"table","grid","","","")
  if name = "ShowHidden" then s = " " & ReplicateBooleanSetting(value,"yes")
  if name = "Controls" then s = "  " & ReplicateBooleanSetting(value,"icons")
  if name = "Refresh" then s = "   " & ReplicateNumericSetting(value,3)
  if name = "ComposeScenes" then s = "      " & ReplicateHexSetting(value)
  if name = "DeviceCutoff" then s = "       " & ReplicateNumericSetting(value,2)
  if name = "EventCutoff" then s = "         " & ReplicateNumericSetting(value,2)
  if name = "LogLines" Then s = "           " & ReplicateNumericSetting(value,3)
  if name = "GridColumns" Then s = "              " & ReplicateNumericSetting(value,1)
  if name = "HideDim" Then s = "               " & ReplicateBooleanSetting(value,"yes")
  if name = "Menus" Then s = "                " & ReplicateBooleanSetting(value,"yes")
  if name = "FrameLinks" Then s = "                 " & ReplicateBooleanSetting(value,"yes")
  if name = "EqualColumns" Then s = "                  " & ReplicateBooleanSetting(value,"yes")
  if name = "NavAtTop" Then s = "                   " & ReplicateBooleanSetting(value,"yes")
  response.write ReplicateSettingsWithOverride(s)
  if name = "Icons" then response.write ReplicateFolderSetting("icons",value) else response.write ReplicateFolderSetting("icons",opt_iconsdir)
  if name = "Snippets" then response.write ReplicateFolderSetting("snippets",value) else response.write ReplicateFolderSetting("snippets",opt_snippetsdir)
  response.write "'>"
  if cstr(actualval) = cstr(value) or (cstr(actualval) = "0" and (value = "none" or value = "full")) then
      response.write "<img src='rover/icons/checked.gif' alt='+' border=0> <b>" & value & "</b>"
    else
      response.write "<img src='rover/icons/unchecked.gif' alt='-' border=0> " & value
    end if
  response.write "</a>&nbsp; "
end sub

' *****************************************************************************
function TrickNumber(name)
  dim i, s, count
  count = cint(hs.GetINISetting("Tricks","Count","0",trick_inifile))
  if count = 0 then exit function
  for i = 1 to count
    s = hs.GetINISetting("Tricks",cstr(i),"",trick_inifile)
    if LCase(name) = LCase(s) then
        TrickNumber = i
        exit function
      end if
    next
  TrickNumber = 0
end function


sub ShowTrickLink(name, iconlead, address)
  dim iconfname
  if opt_style = "grid" then
    iconfname = iconlead & "-" & replace(lcase(name)," ","_") & ".png"
    if FileExists(hs.GetAppPath & "\html\rover\grid\" & iconfname) then 
      response.write "<a href='" & address & "'>" & name & "</a><br>"
      response.write "<a href='" & address & "'><img src='rover/grid/" & iconfname & "' border=0></a>"
    elseif FileExists(hs.GetAppPath & "\html\rover\grid\" & iconlead & ".png") then 
      response.write "<a href='" & address & "'>" & name & "</a><br>"
      response.write "<a href='" & address & "'><img src='rover/grid/" & iconlead & ".png' border=0></a>"
    else
      response.write "<font size='+3'><a href='" & address & "'>" & name & "</a></font>"
    end if
  elseif opt_style = "table" then
    response.write "<td colspan=3><a href='" & address & "'>" & name & "</a></td>"
  else
    response.write "<a href='" & address & "'>" & name & "</a>"
  end if

end sub

' *****************************************************************************
' Show a single trick item.  It's assumed the framing table cell tags etc. are already written.
sub ShowTrickItem(trick, item)
  dim trick_type, trick_name, trick_value, whole_row_start, whole_row_end, dev, devname
  trick_name = hs.GetINISetting("Trick" & cstr(TrickNumber(trick)),cstr(item),"",trick_inifile)
  trick_type = hs.StringItem(trick_name,2,";")
  trick_value = hs.StringItem(trick_name,3,";")
  trick_name = hs.StringItem(trick_name,1,";")
  if opt_style = "grid" then
      whole_row_start = "<font size='+3'>"
      whole_row_end = "</font>"
    elseif opt_style = "table" then
      whole_row_start = "<td colspan=3>"
      whole_row_end = "</td>"
    else
      whole_row_start = ""
      whole_row_end = ""
    end if
  if trick_type = "dev" then
    if hs.DeviceExists(trick_value) = -1 then
      response.write whole_row_start & "Unknown device " & trick_value & whole_row_end
    else
      set dev = hs.GetDeviceByRef(hs.DeviceExistsRef(trick_value))
      if opt_style = "table" then response.write "<td>"
      response.write "<a href='rover.asp?show=dev" & dev.hc & dev.dc & ReplicateSettings() & "'>" & trick_name & "</a>"
      if opt_style = "grid" then
        response.write "<br>"
        WriteDevControlsGrid dev,"trick" & EncodeString(trick),trick_name
        response.write "<br>"
        response.write DevStatus(dev.hc & dev.dc)
      elseif opt_style = "table" then
        response.write "</td><td>"
        response.write DevStatus(dev.hc & dev.dc)
        response.write "</td><td align=center>"
        WriteDevControls dev,"trick" & EncodeString(trick)
        response.write "</td>"
      else
        response.write " ("
        response.write DevStatus(dev.hc & dev.dc)
        response.write ")<br>&nbsp; "
        WriteDevControls dev,"trick" & EncodeString(trick)
      end if
    end if
  elseif trick_type = "evt" then
    ShowTrickLink trick_name, "event", "rover.asp?show=trick" & EncodeString(trick) & "&action=event" & EncodeString(trick_value) & ReplicateSettings()
  elseif trick_type = "link" then
    if opt_framelinks = "yes" then
      ShowTrickLink trick_name, "link", "rover.asp?show=link" & EncodeString(trick_value) & ReplicateSettings()
    else
      ShowTrickLink trick_name, "link", trick_value
    end if
  elseif trick_type = "frame" then
    response.write whole_row_start & "<iframe src='" & trick_value & "' width='100%' height='100%' frameborder=0>"
    response.write "<p>Your browser does not support frames.</p>"
    response.write "<p>Go to your link <a href='" & address & "'>here</a>.</p>"
    response.write "</iframe>" & whole_row_end
  elseif trick_type = "clock" then
    if opt_style = "grid" then
      response.write "<font size='+5'><span id='roverclock'>" & FormatDateTime(Now,vbLongTime) & "</span></font>"
    elseif opt_style = "table" then
      response.write "<td colspan=3><span id='roverclock'>" & FormatDateTime(Now,vbLongTime) & "</span></td>"
    else
      response.write "<span id='roverclock'>" & FormatDateTime(Now,vbLongTime) & "</span>"
    end if
  elseif trick_type = "trick" then
    ShowTrickLink trick_name, "trick", "rover.asp?show=trick" & EncodeString(trick_value) & ReplicateSettings()
  elseif trick_type = "loc" then
    ShowTrickLink trick_name, "location", "rover.asp?show=loc" & EncodeString(trick_value) & ReplicateSettings()
  elseif trick_type = "grp" then
    ShowTrickLink trick_name, "eventgroup", "rover.asp?show=event" & EncodeString(trick_value) & ReplicateSettings()
  else
    response.write whole_row_start & "Unknown item " & trick_name & whole_row_end
  end if
end sub

' *****************************************************************************
' Show a single trick item.  It's assumed the framing table cell tags etc. are already written.
sub ShowTrick(trick)
  dim count, i, thisrow

  response.write "<h3>" & trick & ":</h3>"

  if opt_style = "grid" then
    response.write "<p><table border=0 cols=" & cstr(opt_gridcolumns) & " "
    WriteSnippet "tabletag.html"
    response.write ">"
  elseif opt_style = "table" then
    response.write "<p><table cols=3"
    WriteSnippet "tabletag.html"
    response.write ">"
  end if
  count = cint(hs.GetINISetting("Trick" & cstr(TrickNumber(trick)),"Count","0",trick_inifile))
  if count = 0 then
    if opt_style = "grid" then
      response.write "<tr><td colspan=" & cstr(opt_gridcolumns) & "><i>No items are in this trick.</i></td></tr>"
    elseif opt_style = "table" then
      response.write "<tr><td colspan=3><i>There's nothing to show in this trick.</i></td></tr>"
    else
      response.write "<p><i>There's nothing to show in this trick.</i></p>"
    end if
  else
    thisrow = 0
    for i = 1 to count
      if opt_style = "grid" then
        if thisrow = 0 then response.write "<tr>"
        thisrow = thisrow + 1
        if opt_equalcolumns = "yes" then
            response.write "<td align=center width='" & cstr(100/opt_gridcolumns) & "%'>"
          else
            response.write "<td align=center>"
          end if
      elseif opt_style = "table" then
        response.write "<tr>"
      else
        response.write "<p>"
      end if
      ShowTrickItem trick, i
      if opt_style = "grid" then
        response.write "</td>"
        if thisrow = opt_gridcolumns then 
            response.write "</tr><tr><td colspan=" & cstr(opt_gridcolumns) & ">&nbsp;</td></tr>"
            thisrow = 0
          end if
      elseif opt_style = "table" then
        response.write "</tr>"
      else
        response.write "</p>"
      end if
    next
  end if

  if opt_style = "grid" then
    if thisrow >= 1 then response.write "</tr>"
    response.write "</table></p>"
  elseif opt_style = "table" then
    response.write "</table></p>"
  end if
end sub


function TrickHasAClock(trick)
  dim count, i
  count = cint(hs.GetINISetting("Trick" & cstr(TrickNumber(trick)),"Count","0",trick_inifile))
  for i = 1 to count
    if hs.StringItem(hs.GetINISetting("Trick" & cstr(TrickNumber(trick)),cstr(i),"",trick_inifile),2,";") = "clock" then
       TrickHasAClock = 1
       exit function
     end if
  next
  TrickHasAClock = 0
end function

' *****************************************************************************
sub ShowLog(action)
  dim s, i, j, s2, line
  ' show or clear the HomeSeer log
  response.write "<h3>HomeSeer v" & hs.Version & " Log</h3>"

  ' vet the user
    If GetUserAuthorizations(hs.WebLoggedInUser) <= 1 then
      response.write "<p><b><i>Sorry, log access is available to authorized users only!</i></b></p>"
      hs.WriteLog "Rover","Ignoring log access from " & hs.WebLoggedInUser & " at " & request.ServerVariables("REMOTE_ADDR")
      exit sub
    end If
    
  ' clear the log, or show the clear log link
  if action = 1 then ' clear the log
      hs.ClearLog
      hs.WriteLog "Rover","Cleared the log from " & hs.WebLoggedInUser & " at " & request.ServerVariables("REMOTE_ADDR")
      response.write "<p><b>Cleared the log.</b></p>"
    ElseIf action = 2 Then ' switch the log
      hs.SwitchLog
      hs.WriteLog "Rover","Switched the log from " & hs.WebLoggedInUser & " at " & request.ServerVariables("REMOTE_ADDR")
      response.write "<p><b>Switched the log.</b></p>"
    else ' just show the log
      response.write "<pre>"
      '* old code before hs.LogGet was available:
      's = hs.GetAppPath & "\"
      'If hs.GetINISetting("Settings","gLogDir","False") = "True" Then s = s & "Logs\"
      's = s & hs.GetINISetting("Settings","gLogName","HomeSeer.log")
      's = FileContents(s)
      '* end old code
      s = hs.LogGet
      ' trim to lines
      if opt_loglines <> 0 and action <> 3 Then
          s2 = ""
          if right(s,2) = chr(13) & chr(10) Then s = left(s,len(s)-2)
          for i = 1 to opt_loglines
            ' find a single line
            j = instrRev(s,chr(13) & chr(10))
            if j = 0 Then ' we have fewer lines than the limit anyway
                line = 2
                i = opt_loglines ' to force the loop to end
              else
                ' get line off s, removing it as we go
                line = mid(s,j+2)
                s = left(s,j-1)
              end if
            ' add it to the top of s2
            s2 = line & chr(13) & chr(10) & s2
            Next
          s = s2
        end if
      response.write s
      response.write "</pre><p>Log Functions: <a href='rover.asp?show="
      if opt_loglines <> 0 then 
          if action = 3 then response.write "log" & ReplicateSettings() & "'>Trimmed Log" Else response.write "fulllog" & ReplicateSettings() & "'>Full Log"
          response.write "</a> | <a href='rover.asp?show="
        End if
      response.write "clearlog" & ReplicateSettings() & "'>Clear</a> | <a href='rover.asp?show=switchlog" & ReplicateSettings() & "'>Switch</a></p>"
    end if
end sub


' *****************************************************************************
sub ShowEvents(group)
  ' show a sorted list of events in a given group
  dim evt, events, i, j, k, s, evtname, evtgroup
  dim eventlist(256)
  dim numevents
  dim grouplist(64)
  dim numgroups
  numevents = 0
  numgroups = 0

  set events = hs.GetEventEnumerator
  if IsObject(events) then
      do while not events.Finished
        set evt = events.GetNext
        if not evt is nothing then
            ' if this group's not in the group list, add it
            j = 0
            evtgroup = evt.group
            if evtgroup = "" then evtgroup = "Not Grouped"
            for i = 1 to numgroups
              if LCase(grouplist(i)) = LCase(evtgroup) then j = 1
              next
            if j = 0 then
                numgroups = numgroups + 1
                grouplist(numgroups) = evtgroup
              end if
            ' add to the events list
            if numevents < 256 and LCase(group) <> "groups" and (LCase(group) = LCase(evtgroup) or LCase(group) = "all") then
                numevents = numevents + 1
                eventlist(numevents) = CStr(evt.name)
              end if
          end if
        loop
    end if

  ' sort the list -- simple selection sort
  if numevents > 2 then
      for i = 1 to numevents
        k = i
        for j = i+1 to numevents
          if eventlist(j) < eventlist(k) then k = j
          next
        if k <> i then
            s = eventlist(k)
            eventlist(k) = eventlist(i)
            eventlist(i) = s
          end if
        next
    end if
    
  if opt_style = "grid" then

    if LCase(group) = "all" then
        response.write "<h3>Events in all groups:</h3><p>"
      elseif LCase(group) = "groups" then
        response.write "<h3>Events groups:</h3><p>"
      else
        response.write "<h3>Events in the group """ & group & """:</h3><p>"
      end if

    l = -1 ' cells shown so far
    response.write "<p><table border=0 cols=" & cstr(opt_gridcolumns) & " "
    WriteSnippet "tabletag.html"
    response.write ">"
    if LCase(group) = "groups" then
      for i = 1 to numgroups
        if l <= 0 then response.write "<tr>"
        if l = -1 then l = 0
        l = l + 1 ' we've shown at least something
        if opt_equalcolumns = "yes" then
            response.write "<td align=center width='" & cstr(100/opt_gridcolumns) & "%'>"
          else
            response.write "<td align=center>"
          end if
        response.write "<a href='rover.asp?show=event" & EncodeString(grouplist(i)) & ReplicateSettings() & "'>"
        response.write grouplist(i)
        response.write "</a><br>"
        response.write "<a href='rover.asp?show=event" & EncodeString(grouplist(i)) & ReplicateSettings() & "'><img src='rover/grid/"
        j = "eventgroup-" & replace(lcase(grouplist(i))," ","_") & ".png"
        if FileExists(hs.GetAppPath & "\html\rover\grid\" & j) then 
          response.write j
        else
          response.write "eventgroup.png"
        end if
        response.write "' align=absmiddle border=0 alt='" & grouplist(i) & "'></a>"
        response.write "</td>"
        if l = opt_gridcolumns then 
            response.write "</tr><tr><td colspan=" & cstr(opt_gridcolumns) & ">&nbsp;</td></tr>"
            l = 0
          end if
        next
    else
      for i = 1 to numevents
        if l <= 0 then response.write "<tr>"
        if l = -1 then l = 0
        l = l + 1 ' we've shown at least something
        if opt_equalcolumns = "yes" then
            response.write "<td align=center width='" & cstr(100/opt_gridcolumns) & "%'>"
          else
            response.write "<td align=center>"
          end if
        response.write "<a href='rover.asp?show=event" & EncodeString(group) & "&action=event"
        response.write EncodeString(eventlist(i))
        response.write ReplicateSettings() & "'>"
        evtname = eventlist(i)
        if len(evtname) > opt_eventcutoff and opt_eventcutoff <> 0 then evtname = left(evtname,opt_eventcutoff) & "..."
        response.write evtname
        response.write "</a><br>"
        response.write "<a href='rover.asp?show=event" & EncodeString(group) & "&action=event"
        response.write EncodeString(eventlist(i))
        response.write ReplicateSettings() & "'><img src='rover/grid/"
        j = "event-" & replace(lcase(eventlist(i))," ","_") & ".png"
        if FileExists(hs.GetAppPath & "\html\rover\grid\" & j) then 
          response.write j
        else
          response.write "event.png"
        end if
        response.write "' align=absmiddle border=0 alt='" & eventlist(i) & "'></a>"
        response.write "</td>"
        if l = opt_gridcolumns then 
            response.write "</tr><tr><td colspan=" & cstr(opt_gridcolumns) & ">&nbsp;</td></tr>"
            l = 0
          end if
        next
      end if
    ' finish the last row if necessary
    if l >= 1 then response.write "</tr>"
    ' close the table
    response.write "</table></p>"
        
  else

    if numevents > 0 then
      if LCase(group) = "all" then
          response.write "<h3>Events in all groups:</h3><p>"
        else
          response.write "<h3>Events in the group """ & group & """:</h3><p>"
        end if
        for i = 1 to numevents
          response.write "<a href='rover.asp?show=event" & EncodeString(group) & "&action=event"
          response.write EncodeString(eventlist(i))
          response.write ReplicateSettings() & "'>"
          evtname = eventlist(i)
          if len(evtname) > opt_eventcutoff And opt_eventcutoff <> 0 then evtname = left(evtname,opt_eventcutoff) & "..."
          response.write evtname
          response.write "</a><br>"
          next
        response.write "</p>"
    elseif LCase(group) <> "groups" then
      response.write "<h3>Can't find any events in the group """ & group & """.</h3><p>"
    end if

  end if
 
  ' list of other groups, plus All
  if opt_menus = "no" or opt_style <> "grid" then
      if LCase(group) = "groups" then
        response.write "<h3>Event groups:</h3><p>"
      else
        response.write "<p><small><a href='rover.asp?show=eventGroups" & ReplicateSettings() & "'>Event groups</a>: "
      end if
    for i = 1 to numgroups
      if lcase(group) <> lcase(grouplist(i)) then
          response.write "<a href='rover.asp?show=event"
          response.write EncodeString(grouplist(i))
          response.write ReplicateSettings() & "'>"
          response.write grouplist(i)
          if LCase(group) = "groups" Then
              response.write "</a><br>"
            else
              response.write "</a> | "
            end if
        end if
      next
    if LCase(group) <> "groups" Then
        response.write "<a href='rover.asp?show=eventAll" & ReplicateSettings() & "'>All</a></small></p>"
      end if
    end if

  if opt_menus = "yes" and lcase(group) <> "groups" then
    response.write "<font size='+2'>"
    response.write "<select name='eventgroups' onchange='window.location.href=this.options[this.selectedIndex].value;'>"
    for i = 1 to numgroups
      response.write "<option "
      if lcase(group) = lcase(grouplist(i)) then response.write "selected "
      response.write "value='rover.asp?show=event"
      response.write EncodeString(grouplist(i))
      response.write ReplicateSettings() & "'>"
      response.write grouplist(i)
      response.write "</option>"
      next
    response.write "<option "
    if LCase(group) = "all" then response.write "selected "
    response.write "value='rover.asp?show=eventAll" & ReplicateSettings() & "'>All Groups</option>"
    response.write "</select>"
    response.write "</font> "
  else
  end if
end sub


' *****************************************************************************
sub ShowList()
  dim count, i, s

  ' show a list of locations as links to those locations
  response.write "<h3>Select a location:</h3><p>"

  if opt_style = "grid" then

    l = -1 ' cells shown so far
    response.write "<p><table border=0 cols=" & cstr(opt_gridcolumns) & " "
    WriteSnippet "tabletag.html"
    response.write ">"
    for i = 1 to numlocations
      if l <= 0 then response.write "<tr>"
      if l = -1 then l = 0
      l = l + 1 ' we've shown at least something
      if opt_equalcolumns = "yes" then
          response.write "<td align=center width='" & cstr(100/opt_gridcolumns) & "%'>"
        else
          response.write "<td align=center>"
        end if
      response.write "<a href='rover.asp?show=loc"
      response.write EncodeString(locations(i))
      response.write ReplicateSettings() & "'>"
      response.write locations(i)
      response.write "</a><br>"
      response.write "<a href='rover.asp?show=loc"
      response.write EncodeString(locations(i))
      response.write ReplicateSettings() & "'><img src='rover/grid/"
      j = "location-" & replace(lcase(locations(i))," ","_") & ".png"
      if FileExists(hs.GetAppPath & "\html\rover\grid\" & j) then 
        response.write j
      else
        response.write "location.png"
      end if
      response.write "' align=absmiddle border=0 alt='" & locations(i) & "'></a>"
      response.write "</td>"
      if l = opt_gridcolumns then 
          response.write "</tr><tr><td colspan=" & cstr(opt_gridcolumns) & ">&nbsp;</td></tr>"
          l = 0
        end if
      next
    if l <= 0 then response.write "<tr>"
    if l = -1 then l = 0
    l = l + 1
    if opt_equalcolumns = "yes" then
        response.write "<td align=center width='" & cstr(100/opt_gridcolumns) & "%'>"
      else
        response.write "<td align=center>"
      end if
    response.write "<a href='rover.asp?show=locAll" & ReplicateSettings() & "'>All Locations</a><br>"
    response.write "<a href='rover.asp?show=locAll" & ReplicateSettings() & "'><img src='rover/grid/"
    if FileExists(hs.GetAppPath & "\html\rover\grid\all_locations.png") then 
      response.write "all_locations.png"
    else
      response.write "location.png"
    end if
    response.write "' align=absmiddle border=0 alt='All Locations'></a>"
    response.write "</td>"
    count = 0
    if l = opt_gridcolumns then 
        response.write "</tr><tr><td colspan=" & cstr(opt_gridcolumns) & ">&nbsp;</td></tr>"
        l = 0
      end if
    if tricks_installed then count = cint(hs.GetINISetting("Tricks","Count","0",trick_inifile))
    for i = 1 to count
      if l <= 0 then response.write "<tr>"
      if l = -1 then l = 0
      l = l + 1 ' we've shown at least something
      if opt_equalcolumns = "yes" then
          response.write "<td align=center width='" & cstr(100/opt_gridcolumns) & "%'>"
        else
          response.write "<td align=center>"
        end if
      response.write "<a href='rover.asp?show=trick"
      s = hs.GetINISetting("Tricks",cstr(i),"",trick_inifile)
      response.write EncodeString(s)
      response.write ReplicateSettings() & "'>"
      response.write s
      response.write "</a><br>"
      response.write "<a href='rover.asp?show=trick"
      response.write EncodeString(s)
      response.write ReplicateSettings() & "'><img src='rover/grid/"
      j = "trick-" & replace(lcase(s)," ","_") & ".png"
      if FileExists(hs.GetAppPath & "\html\rover\grid\" & j) then 
        response.write j
      else
        response.write "trick.png"
      end if
      response.write "' align=absmiddle border=0 alt='" & s & "'></a>"
      response.write "</td>"
      if l = opt_gridcolumns then 
          response.write "</tr><tr><td colspan=" & cstr(opt_gridcolumns) & ">&nbsp;</td></tr>"
          l = 0
        end if
      next
    if l = opt_gridcolumns then 
        response.write "</tr><tr><td colspan=" & cstr(opt_gridcolumns) & ">&nbsp;</td></tr>"
        l = 0
      end if
    ' finish the last row if necessary
    if l >= 1 then response.write "</tr>"
    ' close the table
    response.write "</table></p>"

  else

    for i = 1 to numlocations
      response.write "<a href='rover.asp?show=loc"
      response.write EncodeString(locations(i))
      response.write ReplicateSettings() & "'>"
      response.write locations(i)
      response.write "</a><br>"
      next
    response.write "<a href='rover.asp?show=locAll" & ReplicateSettings() & "'>All</a><br>"
    count = 0
    if tricks_installed then count = cint(hs.GetINISetting("Tricks","Count","0",trick_inifile))
    for i = 1 to count
      s = hs.GetINISetting("Tricks",cstr(i),"",trick_inifile)
      response.write "<a href='rover.asp?show=trick"
      response.write EncodeString(s)
      response.write ReplicateSettings() & "'>"
      response.write s
      response.write "</a><br>"
      next
    response.write "</p>"
  end if
end sub


' *****************************************************************************
sub ShowDev(unitcode) 
  ' show details for this device

  ' validate that the device exists and find it
  if hs.DeviceExists(unitcode) = -1 then
      response.write "<p>No device exists at unitcode <b>" & unitcode & "</b>!</p>"
      exit sub
    end if
  set dev = hs.GetDeviceByRef(hs.DeviceExistsRef(unitcode))

  ' show the main details
  response.write "<h3>Details for " & dev.location & " " & dev.name & ":</h3>"
  response.write "<p><b>Device</b>: <a href='rover.asp?show=loc" & EncodeString(dev.location) & ReplicateSettings() & "'>" & dev.location & "</a> " & dev.name & "</p>"
  response.write "<p><b>Address</b>: " & unitcode & " (ref <a href='/deva" & dev.ref & "'>" & dev.ref & "</a>)</p>"

  ' show the device type and details thereabout
  response.write "<p><b>Type</b>: " & dev.dev_type_string & " <small>("
  if dev.can_dim then
      response.write "dimmable"
    else
      response.write "non-dimmable"
    end if
  if dev.status_support then response.write ", supports status report"
  if (dev.misc and &H1) <> 0 then response.write ", preset dim"
  if (dev.misc and &H2) <> 0 then response.write ", extended dim"
  if (dev.misc and &H4) <> 0 then response.write ", SmartLinc"
  if (dev.misc and &H8) <> 0 then response.write ", no logging"
  if (dev.misc and &H10) <> 0 then response.write ", status-only"
  if (dev.misc and &H20) <> 0 then response.write ", hidden"
  if (dev.misc and &H40) <> 0 then response.write ", thermostat"
  if (dev.misc and &H80) <> 0 then response.write ", included in power-fail"
  if (dev.misc and &H100) <> 0 then response.write ", show values"
  if (dev.misc and &H200) <> 0 then response.write ", auto voice command"
  if (dev.misc and &H400) <> 0 then response.write ", confirm voice command"
  if (dev.misc and &H800) <> 0 then response.write ", Compose device"
  if (dev.misc and &H1000) <> 0 then response.write ", Z-Wave"
  if (dev.misc and &H2000) <> 0 then response.write ", other direct-level dim"
  if (dev.misc and &H4000) <> 0 then response.write ", plugin status call"
  if (dev.misc and &H8000) <> 0 then response.write ", plugin value call"
  response.write ")</small></p>"

  ' show the device status
  response.write "<p><b>Status</b>: " & DevStatus(unitcode) & " (since " & DeviceSinceString(unitcode) & ")</p>"

  ' show device controls if appropriate
  if DevRowType(dev) <> 0 Or dev.buttons <> "" then
      response.write "<p><b>Controls</b>: "
      WriteDevControls dev,"dev" & unitcode
      response.write "</p>"
    end if
end sub


' *****************************************************************************
sub ShowLocationGrid(room)

  response.write "<h3>Devices in "
  if lcase(room) = "all" then
      response.write "all locations"
    else
      response.write replace(room,"_"," ")
    end if
  response.write ":</h3>"

  j = hs.DeviceCount
  l = -1 ' cells shown so far
  response.write "<p><table border=0 cols=" & cstr(opt_gridcolumns) & " "
  WriteSnippet "tabletag.html"
  response.write ">"
  thisroom = lcase(Replace(room,"_"," "))
  if thisroom = "no_location" then thisroom = ""

  ' loop through all devices
  set devices = hs.GetDeviceEnumerator
  if IsObject(devices) then
      do while not devices.Finished
        set dev = devices.GetNext
        if not dev is nothing then
            if (opt_showhidden = "yes" or (dev.misc and &H20) = 0) and (lcase(dev.location) = thisroom or thisroom = "all") then
                if l <= 0 then response.write "<tr>"
                if l = -1 then l = 0
                l = l + 1 ' we've shown at least something
                ' show this device
                if opt_equalcolumns = "yes" then
                    response.write "<td align=center width='" & cstr(100/opt_gridcolumns) & "%'>"
                  else
                    response.write "<td align=center>"
                  end if
                if thisroom = "all" then devname = dev.location & " " & dev.name else devname = dev.name
                if len(devname) > opt_devicecutoff And opt_devicecutoff <> 0 then devname = left(devname,opt_devicecutoff) & "..."
                response.write "<a href='rover.asp?show=dev" & dev.hc & dev.dc & ReplicateSettings() & "'>" & devname & "</a><br>"
                WriteDevControlsGrid dev,"loc" & room,devname
                response.write "<br>"
                response.write DevStatus(dev.hc & dev.dc)
                response.write "</td>"
                if l = opt_gridcolumns then 
                    response.write "</tr><tr><td colspan=" & cstr(opt_gridcolumns) & ">&nbsp;</td></tr>"
                    l = 0
                  end if
              end if
          end if
        loop
    end if

  ' finish the last row if necessary
  if l >= 1 then response.write "</tr>"

  ' if we didn't show anything, display that
  if l = -1 then
      response.write "<tr><td colspan=" & cstr(opt_gridcolumns) & "><i>No visible devices are in this location.</i></td></tr>"
    end if

  ' close the table
  response.write "</table></p>"
end sub

' *****************************************************************************
sub ShowLocation(room)
  ' show a location's devices with their controls
  dim dev, devices, devname

  if opt_style = "grid" then
    ShowLocationGrid(room)
    exit sub
  end if

  response.write "<h3>Devices in "
  if lcase(room) = "all" then
      response.write "all locations"
    else
      response.write replace(room,"_"," ")
    end if
  response.write ":</h3>"

  j = hs.DeviceCount
  l = 0 ' shown anything yet?
  if opt_style = "table" then
      response.write "<p><table cols=3"
      WriteSnippet "tabletag.html"
      response.write ">"
    end if
  thisroom = lcase(Replace(room,"_"," "))
  if thisroom = "no_location" then thisroom = ""

  ' loop through all devices
  set devices = hs.GetDeviceEnumerator
  if IsObject(devices) then
      do while not devices.Finished
        set dev = devices.GetNext
        if not dev is nothing then
            if (opt_showhidden = "yes" or (dev.misc and &H20) = 0) and (lcase(dev.location) = thisroom or thisroom = "all") then
                l = 1 ' we've shown at least something
                ' show this device
                if opt_style = "table" then
                    response.write "<tr><td>"
                  else
                    response.write "<p>"
                  end If
                if thisroom = "all" then devname = dev.location & " " & dev.name else devname = dev.name
                if len(devname) > opt_devicecutoff And opt_devicecutoff <> 0 then devname = left(devname,opt_devicecutoff) & "..."
                response.write "<a href='rover.asp?show=dev" & dev.hc & dev.dc & ReplicateSettings() & "'>" & devname & "</a>"
                if opt_style = "table" then
                    response.write "</td><td>"
                  else
                    response.write " ("
                  end if
                response.write DevStatus(dev.hc & dev.dc)
                if opt_style = "table" then
                    response.write "</td><td align=center>"
                  else
                    response.write ")<br>&nbsp; "
                  end if
                WriteDevControls dev,"loc" & room
                if opt_style = "table" then
                    response.write "</td></tr>"
                  else
                    response.write "</p>"
                  end if
              end if
          end if
        loop
    end if

  ' if we didn't show anything, display that
  if l = 0 then
      if opt_style = "table" then
          response.write "<tr><td colspan=3><i>No visible devices are in this location.</i></td></tr>"
        else
          response.write "<p><i>No visible devices are in this location.</i></p>"
        end if
    end if

  ' close the table
  if opt_style = "table" then response.write "</table></p>"
end sub


' *****************************************************************************
sub ShowLink(address)
  response.write "<iframe src='" & address & "' width='100%' height='80%' frameborder=0>"
  response.write "<p>Your browser does not support frames.</p>"
  response.write "<p>Go to your link <a href='" & address & "'>here</a>.</p>"
  response.write "</iframe>"
end sub


' *****************************************************************************
function DevStatus(unitcode)
  ' get the device's status for display, including icons
  s = hs.DeviceString(unitcode)
  if s = "" then
      select case hs.DeviceStatus(unitcode)
        case 2
          s = "<img src='rover/" & opt_iconsdir & "/on.gif' width=16 height=16 align=absmiddle border=0> On"
        case 3
          s = "<img src='rover/" & opt_iconsdir & "/off.gif' width=16 height=16 align=absmiddle border=0> Off"
        case 4
          s = "<img src='rover/" & opt_iconsdir & "/dim.gif' width=16 height=16 align=absmiddle border=0> " & cstr(hs.DeviceValue(unitcode)) & "%"
        case else
          s = "<img src='rover/" & opt_iconsdir & "/unknown.gif' width=16 height=16 align=absmiddle border=0> ??"
        end select
    end if
  DevStatus = s
end function


' *****************************************************************************
function DevRowType(dev)
  ' determine if it's a status-only row (0), on-off row (1), dimmable row (2), thermostat (3), or Compose (4)
  if (dev.misc and &H40) <> 0 then ' thermostat
      DevRowType = 3
    elseif (dev.misc and &H10) <> 0 Or dev.iotype = 4 then ' status only
      DevRowType = 0
    elseif (dev.misc and &H800) <> 0 then ' compose device
      DevRowType = 4
    elseif dev.can_dim and opt_hidedim = "no" then ' dimmable
      DevRowType = 2
    else ' on-off
      DevRowType = 1
    end if
end function


' *****************************************************************************
sub WriteDevControlsGrid(dev,room,devname)
  ' write the controls that apply to this device, depending on its DevRowType
  dim scenename, rowtype

  select case DevRowType(dev)

    case 1 ' on/off devices get only an off and an on button
      WriteDevControlGrid "dev" & dev.hc & dev.dc & "off", room, "off.png", "Off", "Off", devname
      response.write " "
      WriteDevControlGrid "dev" & dev.hc & dev.dc & "on", room, "on.png", "On", "On", devname

    case 2 ' dimmable devices also get buttons for 10% - 90%
      WriteDevControlGrid "dev" & dev.hc & dev.dc & "off", room, "dim-0.png", "Off", "Off", devname
      for k = 10 to 90 step 10
        WriteDevControlGrid "dev" & dev.hc & dev.dc & "abs" & cstr(k), room, "dim-" & cstr(k) & ".png", cstr(k) & "%", cstr(k), devname
        next
      WriteDevControlGrid "dev" & dev.hc & dev.dc & "on", room, "dim-100.png", "On", "On", devname

    case 3 ' thermostats get mode change buttons, and setpoint change buttons
      ' mode buttons
      WriteDevControlGrid "therm" & dev.hc & dev.dc & "off", room, "thermostat-off.png", "Off", "Off", devname
      WriteDevControlGrid "therm" & dev.hc & dev.dc & "cool", room, "thermostat-cool.png", "Cool", "Cool", devname
      WriteDevControlGrid "therm" & dev.hc & dev.dc & "auto", room, "thermostat-auto.png", "Auto", "Auto", devname
      WriteDevControlGrid "therm" & dev.hc & dev.dc & "heat", room, "thermostat-heat.png", "Heat", "Heat", devname
      response.write "<br>"
      ' setpoint buttons
      if not IsNumeric(hs.ControlThermostat(dev.hc & dev.dc,"GetSetPoint",0)) then
          k = 67 ' that's a sensible place to set things relative to
        else
          k = cint(hs.ControlThermostat(dev.hc & dev.dc,"GetSetPoint",0))
        end if
      WriteDevControlGrid "therm" & dev.hc & dev.dc & "set" & cstr(k-5), room, "thermostat-down.png", cstr(k-5) & "�", cstr(k-5), devname
      WriteDevControlGrid "therm" & dev.hc & dev.dc & "set" & cstr(k-2), room, "thermostat-down.png", cstr(k-2) & "�", cstr(k-2), devname
      WriteDevControlGrid "therm" & dev.hc & dev.dc & "set" & cstr(k-1), room, "thermostat-down.png", cstr(k-1) & "�", cstr(k-1), devname
      WriteDevControlGrid "therm" & dev.hc & dev.dc & "set" & cstr(k+1), room, "thermostat-up.png", cstr(k+1) & "�", cstr(k+1), devname
      WriteDevControlGrid "therm" & dev.hc & dev.dc & "set" & cstr(k+2), room, "thermostat-up.png", cstr(k+2) & "�", cstr(k+2), devname
      WriteDevControlGrid "therm" & dev.hc & dev.dc & "set" & cstr(k+5), room, "thermostat-up.png", cstr(k+5) & "�", cstr(k+5), devname

    case 4 ' Compose device
      WriteDevControlGrid "compose" & dev.hc & dev.dc & "on", room, "compose-on.png", "Scene On", "On", devname
      WriteDevControlGrid "compose" & dev.hc & dev.dc & "off", room, "compose-off.png", "Scene Off", "Off", devname
      response.write "<br><select name='compose-" & dev.hc & dev.dc & "' onchange='window.location.href=this.options[this.selectedIndex].value;'><option selected value='#'>Select a scene:</option>"
      for k = 1 to opt_composescenes
        scenename = hs.GetINISetting("Compose","Scene-" & dev.hc & "-" & dev.dc & "-" & cstr(k),"Scene " & chr(asc("A") + k - 1))
        response.write "<option value='rover.asp?action=compose" & dev.hc & dev.dc & "scene"  & cstr(k) & "&show=loc" & room & ReplicateSettings() & "'>" & scenename & "</option>"
        next
      response.write "</select>"

    case else ' status-only or unknown
      if dev.buttons = "" then response.write DeviceSinceString(dev.hc & dev.dc)
    end select

    i = 1
    s = hs.StringItem(dev.buttons,i,chr(1))
    while s <> ""
      WriteDevControlGrid "dev" & dev.hc & dev.dc & "button" & cstr(i), room, replace(lcase(hs.StringItem(s,2,chr(2)))," ","_") & ".png", hs.StringItem(s,2,chr(2)), "[" & hs.StringItem(s,2,chr(2)) & "]", devname
      i = i + 1
      s = hs.StringItem(dev.buttons,i,chr(1))
      wend

end sub


' *****************************************************************************
sub WriteDevControls(dev,room)
  ' write the controls that apply to this device, depending on its DevRowType
  dim scenename, rowtype

  response.write "<small>"

  select case DevRowType(dev)

    case 1 ' on/off devices get only an off and an on button
      WriteDevControl "dev" & dev.hc & dev.dc & "off", room, DevSpecificControlIcon(dev.name,dev.dev_type_string,"off"), "Off", "Off", opt_controls
      response.write " "
      WriteDevControl "dev" & dev.hc & dev.dc & "on", room, DevSpecificControlIcon(dev.name,dev.dev_type_string,"on"), "On", "On", opt_controls

    case 2 ' dimmable devices also get buttons for 10% - 90%
      WriteDevControl "dev" & dev.hc & dev.dc & "off", room, DevSpecificControlIcon(dev.name,dev.dev_type_string,"dim-0"), "Off", "Off", opt_controls
      for k = 10 to 90 step 10
        WriteDevControl "dev" & dev.hc & dev.dc & "abs" & cstr(k), room, "dim-" & cstr(k) & ".gif", cstr(k) & "%", cstr(k), opt_controls
        next
      WriteDevControl "dev" & dev.hc & dev.dc & "on", room, DevSpecificControlIcon(dev.name,dev.dev_type_string,"dim-100"), "On", "On", opt_controls

    case 3 ' thermostats get mode change buttons, and setpoint change buttons
      ' mode buttons
      ThermostatModeControl dev.hc & dev.dc, room, "Off", opt_controls
      ThermostatModeControl dev.hc & dev.dc, room, "Cool", opt_controls
      ThermostatModeControl dev.hc & dev.dc, room, "Auto", opt_controls
      ThermostatModeControl dev.hc & dev.dc, room, "Heat", opt_controls
      response.write " "
      ' setpoint buttons
      if not IsNumeric(hs.GetVar(dev.hc & "setp")) then
          k = 67 ' that's a sensible place to set things relative to
        else
          k = cint(hs.GetVar(dev.hc & "setp"))
        end if
      ThermostatSetpointControl dev.hc & dev.dc, room, k-5, opt_controls
      ThermostatSetpointControl dev.hc & dev.dc, room, k-2, opt_controls
      ThermostatSetpointControl dev.hc & dev.dc, room, k-1, opt_controls
      response.write " "
      ThermostatSetpointControl dev.hc & dev.dc, room, k+1, opt_controls
      ThermostatSetpointControl dev.hc & dev.dc, room, k+2, opt_controls
      ThermostatSetpointControl dev.hc & dev.dc, room, k+5, opt_controls

    case 4 ' Compose device
      WriteDevControl "compose" & dev.hc & dev.dc & "on", room, "compose/on.gif", "Scene On", "On", opt_controls
      for k = 1 to opt_composescenes
        scenename = hs.GetINISetting("Compose","Scene-" & dev.hc & "-" & dev.dc & "-" & cstr(k),"Scene " & chr(asc("A") + k - 1))
        WriteDevControl "compose" & dev.hc & dev.dc & "scene" & cstr(k), room, "compose/scene" & cstr(k) & ".gif", scenename, chr(asc("A") + k - 1), opt_controls
        next
      WriteDevControl "compose" & dev.hc & dev.dc & "off", room, "compose/off.gif", "Scene Off", "Off", opt_controls

    case else ' status-only or unknown
      if dev.buttons = "" then response.write DeviceSinceString(dev.hc & dev.dc)
    end select

    i = 1
    s = hs.StringItem(dev.buttons,i,chr(1))
    while s <> ""
      WriteDevControl "dev" & dev.hc & dev.dc & "button" & cstr(i), room, replace(lcase(hs.StringItem(s,2,chr(2)))," ","_") & ".gif", hs.StringItem(s,2,chr(2)), "[" & hs.StringItem(s,2,chr(2)) & "]", opt_controls
      i = i + 1
      s = hs.StringItem(dev.buttons,i,chr(1))
      wend

  response.write "</small>"

end sub


' *****************************************************************************
function IconExists(basename, icontype)
  dim iconfilename
  iconfilename = Replace(basename," ","_") & "-" & icontype & ".gif"
  if FileExists(hs.GetAppPath & "\html\rover\" & opt_iconsdir & "\" & iconfilename) = 1 then
      IconExists = iconfilename
    else
      IconExists = ""
    end if
end Function


' *****************************************************************************
function DevSpecificControlIcon(name, devtype, icontype)
  dim r, r2
  if icontype = "dim-0" or icontype = "dim-100" then
      r2 = "off"
      if icontype = "dim-100" then r2 = "on"
      r = IconExists(name, icontype)                       'stand_fan-dim-0.gif
      if r = "" then r = IconExists(devtype, icontype)     'fan-dim-0.gif
      if r = "" then r = IconExists(name, r2)              'stand_fan-off.gif
      if r = "" then r = IconExists(devtype, r2)           'fan-off.gif
      if r = "" then r = icontype & ".gif"                 'dim-0.gif
      if FileExists(hs.GetAppPath & "\html\rover\" & opt_iconsdir & "\" & r) = 0 then r = r2 & ".gif" 'off.gif
    elseif icontype = "off" or icontype = "on" then
      r = IconExists(name, icontype)                       'stand_fan-off.gif
      if r = "" then r = IconExists(devtype, icontype)     'fan-off.gif
      if r = "" then r = icontype & ".gif"                 'off.gif
    else
      r = icontype
    end if
  DevSpecificControlIcon = r
end function


' *****************************************************************************
sub WriteDevControlGrid(url, room, icon, alt, text, devname)
  response.write "<a href='rover.asp?action=" & url & "&show=" & room & ReplicateSettings() & "'>"
  if FileExists(hs.GetAppPath & "\html\rover\grid\dev-" & replace(lcase(devname)," ","_") & "-" & icon) then
      response.write "<img src='rover/grid/dev-" & replace(lcase(devname)," ","_") & "-" & icon & "' align=absmiddle border=0 alt='" & alt & "'></a>"
    elseif FileExists(hs.GetAppPath & "\html\rover\grid\" & icon) then
      response.write "<img src='rover/grid/" & icon & "' align=absmiddle border=0 alt='" & alt & "'></a>"
    else
      response.write text & "</a>&nbsp;"
    end if
end sub

' *****************************************************************************
sub WriteDevControl(url, room, icon, alt, text, controltype)
  response.write "<a href='rover.asp?action=" & url & "&show=" & room & ReplicateSettings() & "'>"
  if controltype = "icons" and FileExists(hs.GetAppPath & "\html\rover\" & opt_iconsdir & "\" & icon) then
      response.write "<img src='rover/" & opt_iconsdir & "/" & icon & "' align=absmiddle border=0 alt='" & alt & "'></a>"
    else
      response.write text & "</a>&nbsp;"
    end if
end sub

' *****************************************************************************
sub ThermostatModeControl(unitcode, room, mode, controltype)
  ' display a single thermostat mode change control
  WriteDevControl "therm" & unitcode & lcase(mode), room, "thermostats/mode-" & lcase(mode) & ".gif", mode, mode, controltype
end sub


' *****************************************************************************
sub ThermostatSetpointControl(unitcode, room, newsetpoint, controltype)
  ' display a single thermostat setpoint change control
  WriteDevControl "therm" & unitcode & "set" & cstr(newsetpoint), room, "thermostats/" & cstr(newsetpoint) & ".gif", cstr(newsetpoint) & "�", cstr(newsetpoint), controltype
end sub


' *****************************************************************************
sub BottomLinks(qshow)
  dim count, s
  ' display the links that go on the bottom of every page
  if opt_menus = "yes" then

    response.write "<table border=0 width='100%'><tr><td><font size='+2'>"
    WriteSnippet "menulinks1.html"
    if qshow <> "list" then
      response.write "<select name='show' onchange='window.location.href=this.options[this.selectedIndex].value;'>"
      if left(qshow,3) <> "loc" then response.write "<option selected value='#'>Location:</option>"
      for i = 1 to numlocations
        response.write "<option "
        if lcase(qshow) = "loc" & Replace(lcase(locations(i))," ","_") then response.write "selected "
        response.write "value='rover.asp?show=loc"
        response.write EncodeString(locations(i))
        response.write ReplicateSettings() & "'>"
        response.write locations(i)
        response.write "</option>"
        next
      response.write "<option "
      if lcase(qshow) = "locall" then response.write "selected "
      response.write "value='rover.asp?show=locAll"
      response.write EncodeString(locations(i))
      response.write ReplicateSettings() & "'>All Locations</option>"
      if tricks_installed then count = cint(hs.GetINISetting("Tricks","Count","0",trick_inifile))
      for i = 1 to count
        s = hs.GetINISetting("Tricks",cstr(i),"",trick_inifile)
        response.write "<option value='rover.asp?show=trick"
        response.write EncodeString(s)
        response.write ReplicateSettings() & "'>"
        response.write s
        response.write "</option>"
        next
      response.write "</select> "
      end if
    WriteSnippet "menulinks2.html"
    response.write "</font></td><td align=center><font size='+2'>"
    WriteSnippet "menulinks-" & replace(replace(lcase(qshow)," ",""),"_","") & ".html"
    response.write "</font></td><td align=right><font size='+2'><select name='system' onchange='window.location.href=this.options[this.selectedIndex].value;'>"
    response.write "<option selected value='rover.asp?show=about" & ReplicateSettings() & "'>Rover v" & rover_version & "</option>"
    response.write "<option value='rover.asp?show=list" & ReplicateSettings() & "'>Locations</option>"
    response.write "<option value='rover.asp?show=eventGroups" & ReplicateSettings() & "'>Events</option>"
    response.write "<option value='rover.asp?show=options" & ReplicateSettings() & "'>Options</option>"
    response.write "<option value='rover.asp?show=log" & ReplicateSettings() & "'>Log</option>"
    response.write "<option value='rover/manual.html'>Manual</option>"
    if tricks_installed then 
        response.write "<option value='rover/tricks/rover_tricks.asp'>Tricks Editor</option>"
      else
        response.write "<option value='http://rover-for-homeseer.blogspot.com/2011/04/rover-tricks-editor.html'>Tricks</option>"
      end if
    response.write "<option value='rover.asp?show=about" & ReplicateSettings() & "'>About</option>"
    response.write "<option value='/'>Exit</option>"
    response.write "</select></font></td></tr></table></font>"

  else

    response.write "<p><small>"
    if qshow <> "list" then
      for i = 1 to numlocations
        if lcase(qshow) <> "loc" & Replace(lcase(locations(i))," ","_") then
            response.write "<a href='rover.asp?show=loc"
            response.write EncodeString(locations(i))
            response.write ReplicateSettings() & "'>"
            response.write locations(i)
            response.write "</a> | "
          end if
        next
      end if
    response.write "<a href='rover.asp?show=locAll" & ReplicateSettings() & "'>All</a> | "
    if tricks_installed then count = cint(hs.GetINISetting("Tricks","Count","0",trick_inifile))
    for i = 1 to count
      s = hs.GetINISetting("Tricks",cstr(i),"",trick_inifile)
      response.write "<a href='rover.asp?show=trick"
      response.write EncodeString(s)
      response.write ReplicateSettings() & "'>"
      response.write s
      response.write "</a> | "
      next
    response.write "<a href='rover.asp?show=eventGroups" & ReplicateSettings() & "'>Events</a> | "
    response.write "<a href='rover.asp?show=log" & ReplicateSettings() & "'>Log</a> | "
    WriteSnippet "links.html"
    response.write "<a href='rover.asp?show=options" & ReplicateSettings() & "'>Options</a> | "
    if tricks_installed then 
        response.write "<a href='rover/tricks/rover_tricks.asp'>Tricks Editor</a> | "
      else
        response.write "<a href='http://rover-for-homeseer.blogspot.com/2011/04/rover-tricks-editor.html'>Tricks</option> | "
      end if
    response.write "<a href='rover.asp?show=about" & ReplicateSettings() & "'>About</a></small></p>"

  end if
end sub


' *****************************************************************************
function SinceString(seconds)
  ' Given an time in seconds, builds a string showing the "since" time, handling "Today at" etc.
  dim since, d, h, m, s, ampm
  if seconds = 0 then
      SinceString = "Now"
      exit function
    elseif seconds = 1 then
      SinceString = "1 second ago"
      exit function
    elseif seconds < 60 then
      SinceString = seconds & " seconds ago"
      exit function
    elseif seconds < 120 then
      SinceString = "1 minute ago"
      exit function
    elseif seconds < 3600 then
      SinceString = Int(seconds/60) & " minutes ago"
      exit function
    end if
  since = Now - seconds/86400
  select case Int(Now) - Int(since)
    case 7, 6, 5, 4, 3, 2
      d = WeekdayName(Weekday(since))
    case 1
      d = "Yesterday"
    case 0
      d = "Today"
    case else
      d = Year(since) & "-" & right("00" & Month(since),2) & "-" & right("00" & Day(since),2)
    end select
  if Hour(since) >= 12 then
      h = Hour(since) - 12
      ampm = "pm"
    else
      h = Hour(since)
      ampm = "am"
    end if
  if h = 0 then h = 12
  m = right("00" & Trim(Minute(since)),2)
  SinceString = d & " at " & h & ":" & m & ampm
end function


' *****************************************************************************
function DeviceSinceString(itemcode)
  ' returns a string for how long the device has been in this state
  DeviceSinceString = SinceString(datediff("s",cdate(hs.DeviceLastChange(itemcode)),now))
end function


' *****************************************************************************
function ReplicateSettingsOptionsOnly()
  ' returns the parameters necessary to set all the current settings
  ReplicateSettingsOptionsOnly = "&options=" & ReplicateTupleSetting(opt_style,"table","grid","","","") & ReplicateBooleanSetting(opt_showhidden,"yes") & ReplicateBooleanSetting(opt_controls,"icons") & ReplicateNumericSetting(opt_refresh,3) & ReplicateHexSetting(opt_composescenes) & ReplicateNumericSetting(opt_devicecutoff,2) & ReplicateNumericSetting(opt_eventcutoff,2) & ReplicateNumericSetting(opt_loglines,3) & ReplicateNumericSetting(opt_gridcolumns,1) & ReplicateBooleanSetting(opt_hidedim,"yes") & ReplicateBooleanSetting(opt_menus,"yes") & ReplicateBooleanSetting(opt_framelinks,"yes") & ReplicateBooleanSetting(opt_equalcolumns,"yes") & ReplicateBooleanSetting(opt_navattop,"yes")
end function


' *****************************************************************************
function ReplicateSettings()
  ' returns the parameters necessary to set all the current settings
  dim r
  r = ReplicateSettingsOptionsOnly()
  if opt_iconsdir <> "icons" then r = r & "&icons=" & opt_iconsdir
  if opt_snippetsdir <> "snippets" then r = r & "&snippets=" & opt_snippetsdir
  ReplicateSettings = r
end function


' *****************************************************************************
function ReplicateBooleanSetting(actualval, oneval)
  if actualval = oneval then ReplicateBooleanSetting = "1" else ReplicateBooleanSetting = "0"
end function


' *****************************************************************************
function ReplicateTupleSetting(actualval, oneval, twoval, threeval, fourval, fiveval)
  ReplicateTupleSetting = "0"
  if actualval = oneval then ReplicateTupleSetting = "1" 
  if actualval = twoval then ReplicateTupleSetting = "2" 
  if actualval = threeval then ReplicateTupleSetting = "3" 
  if actualval = fourval then ReplicateTupleSetting = "4" 
  if actualval = fiveval then ReplicateTupleSetting = "5" 
end function


' *****************************************************************************
function ReplicateNumericSetting(actualval, digits)
  if not isNumeric(actualval) then ReplicateNumericSetting = right("000000",digits) Else ReplicateNumericSetting = right("000000" & cstr(actualval),digits)
end function


' *****************************************************************************
function ReplicateHexSetting(actualval)
  if actualval >= 10 then ReplicateHexSetting = chr(asc("A") + actualval - 10) else ReplicateHexSetting = cstr(actualval)
end function


' *****************************************************************************
function ReplicateSettingsWithOverride(mask)
  ' overrides the replication of specified settings
  ' the mask is spaces for non-overridden characters
  dim s, i, c
  s = ReplicateSettingsOptionsOnly()
  For i = 1 To Len(mask)
    c = mid(mask,i,1)
    if c <> " " then s = left(s,i+8) & c & mid(s,i+10)
    Next
  ReplicateSettingsWithOverride = s
end function


' *****************************************************************************
sub WriteSnippet(filename)
  ' writes a snippet file, adding settings if necessary to the URLs
  dim s, delimiter, surl, i
  dim endStr, devCode, status, source
  dim houseCode, count
  s = FileContents(hs.GetAppPath & "\html\rover\" & opt_snippetsdir & "\" & filename)
  s = replace(s,"$time$",FormatDateTime(Now,vbLongTime))
  s = replace(s,"$military$",FormatDateTime(Now,vbShortTime))
  s = replace(s,"$date$",FormatDateTime(Now,vbShortDate))
  s = replace(s,"$longdate$",FormatDateTime(Now,vbLongDate))
  s = replace(s,"$rover_version$",rover_version)
  s = replace(s,"$icons$",opt_iconsdir)
  s = replace(s,"$snippets$",opt_snippetsdir)
  s = replace(s,"$style$",opt_style)
  s = replace(s,"$gridcolumns$",opt_gridcolumns)
  s = replace(s,"$equalcolumns$",opt_equalcolumns)
  s = replace(s,"$navattop$",opt_navattop)
  s = replace(s,"$showhidden$",opt_showhidden)
  s = replace(s,"$hidedim$",opt_hidedim)
  s = replace(s,"$menus$",opt_menus)
  s = replace(s,"$framelinks$",opt_framelinks)
  s = replace(s,"$controls$",opt_controls)
  s = replace(s,"$refresh$",opt_refresh)
  s = replace(s,"$composescenes$",opt_composescenes)
  s = replace(s,"$devicecutoff$",opt_devicecutoff)
  s = replace(s,"$eventcutoff$",opt_eventcutoff)
  s = replace(s,"$loglines$",opt_loglines)
  s = replace(s,"$hs_version$",hs.Version)
  s = replace(s,"$hs_uptime$",hs.SystemUptime)
  s = replace(s,"$hs_emails$",hs.MailMsgCount)
  s = replace(s,"$sunrise$",hs.Sunrise)
  s = replace(s,"$sunset$",hs.Sunset)
  s = replace(s,"$options$",ReplicateSettings())
  ' More complex replace, by Tony McNamara.
  ' Replaces $devstat{unit code}$ with device status
  i = instr(s, "$devstat")
  count = 0
  do while ((i > 0) and (count < 30))
        count = count + 1
		endStr = i+8
		houseCode = mid(s, endStr, 1)
		endStr = endStr+1
		devCode = mid(s, endStr, 1)
		endStr = endStr+1
		if (mid(s, endStr, 1)<>"$") then
			devCode = devCode & mid(s, endStr, 1)
			endStr = endStr+1
		end if
		status = GetDeviceStatus(houseCode&devCode)
		source = "$devstat"&houseCode&devCode&"$"
		' response.write "replacing " & source & " with " & status
		s = replace(s, source, status)
	i = instr(s, "$devstat")
	loop
  s = replace(s,"$$","$")
  
  do
    i = instr(s,"<a href=") ' hopefully no one will use <a name='wombat' href='blah.asp'> but then why would they?
    if i = 0 then exit do ' there's no more URLs to be processed
    if i <> 1 then ' first handle the stuff to the left of it
        response.write left(s,i-1)
        s = mid(s,i)
      end if
    ' now the string starts with it
    s = mid(s,9) ' trim off the "<a href=" part
    ' figure out what delimiter we want for the URL
    delimiter = left(s,1)
    if delimiter <> chr(34) and delimiter <> "'" then delimiter = ">"
    i = instr(2,s,delimiter)
    if i = 0 then exit do ' incomplete tag; let's just dump it and go
    ' chop out the url
    surl = mid(s,2,i-2)
    s = mid(s,i+1)
    ' surl now holds the URL part
    ' add options to it
    if instr(lcase(surl),"rover.asp") <> 0 then surl = surl & ReplicateSettings()
    ' rebuild the <a> tag and write it
    response.write "<a href='" & surl & "'"
    if delimeter = ">" then response.write ">"
    ' otherwise s has the rest of the tag, including the ">"
    loop
  response.write s ' whatever is left over
end sub


' *****************************************************************************
function FileExists(filename)
  dim FSO
  Set FSO = CreateObject("Scripting.FileSystemObject")
  FileExists = 0
  if FSO.FileExists(filename) Then FileExists = 1
end function


' *****************************************************************************
function FileContents(filename)
  ' fetch the contents of a file (used for plug-in files)
  dim contents, FSO, TextStream
  Set FSO = CreateObject("Scripting.FileSystemObject")
  if FSO.FileExists(filename) then
      Set TextStream = FSO.OpenTextFile(filename, 1)
      contents = TextStream.ReadAll
      TextStream.Close
    else
      contents = ""
    end if
  FileContents = contents
end function


' *****************************************************************************
function EncodeString(s)
  ' converts a string to URL-encoding (e.g., Frank%26s_Room)
  dim i, c, result
  result = ""
  for i = 1 to len(s)
    c = mid(s,i,1)
    if c = " " then
        result = result & "_"
      elseif c = "." or c = "-" then
        result = result & c
      elseif c < "0" or (c > "9" and c < "A") or (c > "Z" and c < "a") or (c > "z") then
        result = result & "%" & hex(asc(c))
      else
        result = result & c
      end if
    next
  EncodeString = result
end function

function DecodeString(s)
  ' reverses the above
  dim i, c, result
  result = ""
  for i = 1 to len(s)
    c = mid(s,i,1)
    if c = "%" then
        result = result & chr(mid(s,i+1,2))
        i = i + 2
      else
        result = result & c
      end if
    next
  DecodeString = result
end function

' *****************************************************************************
function GetUserAuthorizations(username)
  dim sUserList, sUsers, sUser, sUsername, sRights, i
  sUserList = hs.GetUsers
  sUsers = Split(sUserList,",")
  for i = 0 to UBound(sUsers)
    sUser = sUsers(i)
    sUsername = left(sUser,instr(sUser,"|")-1)
    sRights = cint(trim(mid(sUser,instr(sUser,"|")+1)))
    if sUsername = username then
        GetUserAuthorizations = sRights
        exit function
      end if
    next
  GetUserAuthorizations = 0
end function

' *****************************************************************************
sub hs_install()
  ' nothing to do
end sub

sub hs_uninstall()
  ' nothing to do
end sub

' *****************************************************************************
function GetDeviceStatus(unitCode)
  ' Written by Tony McNamara, 4 Aug 07
  ' Provides the ShowLocation() type detail for a single device
  ' The return is just a graphic and text
  
  dim dev, devices, devname, success
	dim curCode 
	
  j = hs.DeviceCount
  l = 0 ' shown anything yet?
  success = 0

  ' loop through all devices
  set devices = hs.GetDeviceEnumerator
	if IsObject(devices) then
		do while ((not devices.Finished) and (success = 0))
			set dev = devices.GetNext
			if not dev is nothing then
				' response.write "Code:" & dev.hc & dev.dc & " Name: " & dev.name
				curCode = dev.hc&dev.dc
				if (lcase(curCode) = lcase(unitCode)) then
					GetDeviceStatus = DevStatus(dev.hc & dev.dc)
					success = 1
				End if
			end if
        loop
    End if
    if (success = 0) then
        GetDeviceStatus = "Device " & unitCode & " not found"	
    end if
end function




%>
