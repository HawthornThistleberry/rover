<%@ LANGUAGE=VBScript %>
<html>
<head>
<title>Rover Tricks Editor For HomeSeer</title>

<%
' Rover Tricks Editor: virtual location creator for Rover
' (c) 2011 by Frank J. Perricone, hawthorn@foobox.com
' see rover\tricks\manual.txt for installation, customization, and other docs

' *****************************************************************************
dim opt_trick, opt_action, opt_edit, opt_name
dim rte_version
dim inifile

rte_version = "1.1"
inifile = "rover_tricks.ini"

opt_trick = WordToName(request.querystring("trick"))
if opt_trick = "" then opt_trick = "none"
opt_action = request.querystring("action")
if opt_action = "" then opt_action = "tricklist"
opt_edit = request.querystring("edit")
opt_name = request.querystring("name")

' build the page header
response.write "<style type='text/css'>"
response.write "a {text-decoration:none}"
response.write "</style>"
response.write "</head><body><p><a href='manual.html'><img src='../icons/rover.gif' width=32 height=32 align=right border=0></a><strong>Rover Tricks Editor v" & rte_version & "</strong>: You <em>can</em> teach an old dog new tricks!</p><hr>"

if GetUserAuthorizations(hs.WebLoggedInUser) <= 1 then
    response.write "<p><b><i>Sorry, Rover Tricks Editor is available to authorized users only!</i></b></p>"
    hs.WriteLog "RoverTricks","Ignoring action from " & hs.WebLoggedInUser & " at " & request.ServerVariables("REMOTE_ADDR")
  else
    if opt_action = "tricklist" then '?trick=none&action=tricklist
        Screen_TrickList
      elseif opt_action = "new" then '?trick=none&action=new
        Screen_NewTrick
      elseif opt_trick = "none" then
        response.write "<p><font color=red>Error: Invalid syntax.  Please return to the <a href='rover_tricks.asp?action=tricklist'>trick list</a>.</font></p>"
      elseif opt_action = "show" then '?trick=<trick>&action=show[&edit=<edit>[&name=<newname>]]
        Screen_ShowTrick opt_trick, opt_edit, opt_name
      elseif opt_action = "rename" and opt_name = "" then '?trick=<trick>&action=rename
        Screen_RenameTrick opt_trick
      elseif opt_action = "rename" then '?trick=<trick>&action=rename&name=<newname>
        Screen_RenamedTrick opt_trick, opt_name
      elseif opt_action = "delete" then '?trick=<trick>&action=delete
        Screen_DeleteTrick opt_trick
      elseif opt_action = "deleted" then '?trick=<trick>&action=deleted
        Screen_DeletedTrick opt_trick
      elseif opt_action = "adddevice" then '?trick=<trick>&action=adddevice
        Screen_AddDevice opt_trick
      elseif opt_action = "addevent" then '?trick=<trick>&action=addevent
        Screen_AddEvent opt_trick
      elseif opt_action = "addlink" then '?trick=<trick>&action=addlink
        Screen_AddLink opt_trick, 0
      elseif opt_action = "addframe" then '?trick=<trick>&action=addframe
        Screen_AddLink opt_trick, 1
      elseif opt_action = "addsnippet" then '?trick=<trick>&action=addsnippet
        Screen_AddLink opt_trick, 2
      elseif opt_action = "addtrick" then '?trick=<trick>&action=addtrick
        Screen_AddTrick opt_trick
      elseif opt_action = "addloc" then '?trick=<trick>&action=addloc
        Screen_AddLoc opt_trick
      elseif opt_action = "addgrp" then '?trick=<trick>&action=addgrp
        Screen_AddGroup opt_trick
      elseif left(opt_action,6) = "rename" then '?trick=<trick>&action=rename<item>
        Screen_RenameItem opt_trick, mid(opt_action,7)
      else
        response.write "<p><font color=red>Error: Invalid syntax.  Please return to the <a href='rover_tricks.asp?action=tricklist'>trick list</a>.</font></p>"
      end if
  end if

response.write "</body></html>"

' end of main routine -- everything else is functions and subs

function NameToWord(s)
  NameToWord = replace(s," ","_")
end function

function WordToName(s)
  WordToName = replace(s,"_"," ")
end function

function FirstMeaningfulWordFromURL(s)
  dim i, wordstring, oneword
  wordstring = replace(s,":"," ")
  wordstring = replace(wordstring,"/"," ")
  wordstring = replace(wordstring,"\"," ")
  wordstring = replace(wordstring,"."," ")
  wordstring = replace(wordstring,","," ")
  wordstring = replace(wordstring,"-"," ")
  wordstring = replace(wordstring,"_"," ")
  wordstring = replace(wordstring,"?"," ")
  wordstring = replace(wordstring,"&"," ")
  wordstring = replace(wordstring,"#"," ")
  wordstring = replace(wordstring,"$"," ")
  wordstring = replace(wordstring,"%"," ")
  wordstring = replace(wordstring,"="," ")
  wordstring = replace(wordstring,"+"," ")
  wordstring = replace(wordstring,"@"," ")
  wordstring = replace(wordstring,"^"," ")
  for i = 1 to 10
    wordstring = replace(wordstring,"  "," ")
    next
  i = 1
  oneword = hs.StringItem(wordstring,i," ")
  while oneword <> ""
    if oneword <> "www" and oneword <> "http" and oneword <> "web" then
        FirstMeaningfulWordFromURL = oneword
        exit function
      end if
    i = i + 1
    oneword = hs.StringItem(wordstring,i," ")
    wend
  oneword = hs.StringItem(wordstring,1," ")
  if oneword = "" then oneword = "URL"
  FirstMeaningfulWordFromURL = oneword
end function

function TrickNumber(name)
  dim i, s, count
  count = cint(hs.GetINISetting("Tricks","Count","0",inifile))
  if count = 0 then exit function
  for i = 1 to count
    s = hs.GetINISetting("Tricks",cstr(i),"",inifile)
    if LCase(name) = LCase(s) then
        TrickNumber = i
        exit function
      end if
    next
  TrickNumber = 0
end function

function AddItemToTrick(trick, itemname, itemtype, itemvalue)
  dim tricknum, count
  ' if the trick doesn't exist yet, create it
  tricknum = TrickNumber(trick)
  if tricknum = 0 then
    tricknum = cint(hs.GetINISetting("Tricks","Count","0",inifile))
    tricknum = tricknum + 1
    hs.SaveINISetting "Tricks","Count",cstr(tricknum),inifile
    hs.SaveINISetting "Tricks",cstr(tricknum),trick,inifile
    hs.SaveINISetting "Trick" & cstr(tricknum),"Count","0",inifile
  end if
  ' add the item
  count = cint(hs.GetINISetting("Trick" & cstr(tricknum),"Count","0",inifile))
  count = count + 1
  hs.SaveINISetting "Trick" & cstr(tricknum),"Count",cstr(count),inifile
  hs.SaveINISetting "Trick" & cstr(tricknum),cstr(count),itemname & ";" & itemtype & ";" & itemvalue,inifile
  AddItemToTrick = count
end function

sub DeleteItemFromTrick(trick, itemnumber)
  dim count, i, tricknum
  ' if the trick doesn't exist yet, just exit
  tricknum = TrickNumber(trick)
  if tricknum = 0 then exit sub
  count = cint(hs.GetINISetting("Trick" & cstr(tricknum),"Count","0",inifile))
  if itemnumber > count then exit sub
  if itemnumber < count then
    for i = itemnumber to count-1
      hs.SaveINISetting "Trick" & cstr(tricknum),cstr(i),hs.GetINISetting("Trick" & cstr(tricknum),cstr(i+1),"",inifile),inifile
      next
  end if
  hs.SaveINISetting "Trick" & cstr(tricknum),cstr(count),"",inifile
  hs.SaveINISetting "Trick" & cstr(tricknum),"Count",cstr(count-1),inifile
end sub

' Swaps items itemnumber and itemnumber+1
sub SwapItemsInTrick(trick, itemnumber)
  dim count, val1, tricknum
  ' if the trick doesn't exist yet, just exit
  tricknum = TrickNumber(trick)
  if tricknum = 0 then exit sub
  count = cint(hs.GetINISetting("Trick" & cstr(tricknum),"Count","0",inifile))
  if itemnumber >= count then exit sub
  val1 = hs.GetINISetting("Trick" & cstr(tricknum),cstr(itemnumber),"",inifile)
  hs.SaveINISetting "Trick" & cstr(tricknum),cstr(itemnumber),hs.GetINISetting("Trick" & cstr(tricknum),cstr(itemnumber+1),"",inifile),inifile
  hs.SaveINISetting "Trick" & cstr(tricknum),cstr(itemnumber+1),val1,inifile
end sub

sub DeleteATrick(tricknum)
  dim count, i
  if tricknum = 0 then exit sub
  count = cint(hs.GetINISetting("Tricks","Count","0",inifile))
  if tricknum > count then exit sub
  if tricknum < count then
    for i = tricknum to count-1
      hs.SaveINISetting "Tricks",cstr(i),hs.GetINISetting("Tricks",cstr(i+1),"",inifile),inifile
      next
  end if
  hs.SaveINISetting "Tricks",cstr(count),"",inifile
  hs.SaveINISetting "Tricks","Count",cstr(count-1),inifile
  hs.ClearINISection "Trick" & cstr(tricknum), inifile
end sub

sub DisplayTrickItemDetails(s)
  if hs.StringItem(s,2,";") = "dev" then
      if hs.DeviceExists(hs.StringItem(s,3,";")) = -1 then
        response.write "device code " & hs.StringItem(s,3,";") & ": no such device"
      else
        set dev = hs.GetDeviceByRef(hs.DeviceExistsRef(hs.StringItem(s,3,";")))
        response.write "device code " & hs.StringItem(s,3,";") & ": " & dev.name
      end if
    elseif hs.StringItem(s,2,";") = "evt" then
      response.write "event " & hs.StringItem(s,3,";")
    elseif hs.StringItem(s,2,";") = "link" then
      response.write "link to <a href='" & hs.StringItem(s,3,";") & "'>" & hs.StringItem(s,3,";") & "</a>"
    elseif hs.StringItem(s,2,";") = "frame" then
      response.write "framed link to <a href='" & hs.StringItem(s,3,";") & "'>" & hs.StringItem(s,3,";") & "</a>"
    elseif hs.StringItem(s,2,";") = "snippet" then
      response.write "snippet link to <a href='" & hs.StringItem(s,3,";") & "'>" & hs.StringItem(s,3,";") & "</a>"
    elseif hs.StringItem(s,2,";") = "clock" then
      response.write "clock"
    elseif hs.StringItem(s,2,";") = "trick" then
      response.write "trick " & hs.StringItem(s,3,";")
    elseif hs.StringItem(s,2,";") = "loc" then
      response.write "location " & hs.StringItem(s,3,";")
    elseif hs.StringItem(s,2,";") = "grp" then
      response.write "event group " & hs.StringItem(s,3,";")
    else
      response.write "unknown item"
    end if
end sub

sub Screen_TrickList()
  dim i, s, count
  response.write "<h3>Tricks:</h3>"
  count = cint(hs.GetINISetting("Tricks","Count","0",inifile))
  if count = 0 then
      response.write "<i>You have no tricks defined yet.</i><br>"
    else
      response.write "<ul>"
      for i = 1 to count
        s = hs.GetINISetting("Tricks",cstr(i),"",inifile)
        response.write "<li><a href='rover_tricks.asp?trick=" & NameToWord(s) & "&action=show'>" & s & "</a>"
        response.write " (" & hs.GetINISetting("Trick" & cstr(i),"Count",0,inifile) & " items)</li>"
        next
      response.write "</ul>"
    end if
  response.write "<hr>"
  response.write "<table border=0 width='100%'><tr><td><a href='rover_tricks.asp?trick=none&action=new'>Create a New Trick</a></td><td align=right><a href='../../rover.asp'>Exit</a></td></tr></table>"
end sub

sub Screen_NewTrick()
  response.write "<p>Give your new trick a name:</p>"
  response.write "<form action='rover_tricks.asp' method=get><input type='text' name='trick'><input type='hidden' name='action' value='show'><input type='submit' value='Create'></form>"
  response.write "<p align=right><a href='rover_tricks.asp?trick=none&action=tricklist'>Exit</a></p>"
end sub

sub Screen_ShowTrick(trick, edit, name)
  dim tricknum, count, i, dev
  ' if the trick doesn't exist yet, create it
  tricknum = TrickNumber(trick)
  'response.write "Showing trick number " & cstr(tricknum) & " (" & trick & ") with edit=" & edit & ", name=" & name
  if tricknum = 0 then
    tricknum = cint(hs.GetINISetting("Tricks","Count","0",inifile))
    tricknum = tricknum + 1
    hs.SaveINISetting "Tricks","Count",cstr(tricknum),inifile
    hs.SaveINISetting "Tricks",cstr(tricknum),trick,inifile
    hs.SaveINISetting "Trick" & cstr(tricknum),"Count","0",inifile
  end if
  ' handle edits
  if edit <> "" then HandleEdits trick, edit, name
  ' show the trick
  response.write "<h3>Trick: " & trick & "</h3>"
  count = cint(hs.GetINISetting("Trick" & cstr(tricknum),"Count","0",inifile))
  if count = 0 then
      response.write "<i>This trick has no items yet.</i><br>"
    else
      response.write "<ol>"
      for i = 1 to count
        s = hs.GetINISetting("Trick" & cstr(tricknum),cstr(i),"",inifile)
        response.write "<li><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=rename" & cstr(i) & "'><img src='rename.png' alt='Rename' border=0 align=absmiddle></a> "
        response.write "<a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show&edit=del" & cstr(i) & "'><img src='delete.png' alt='Delete' border=0 align=absmiddle></a> "
        if i = 1 then 
            response.write "<img src='nothing.png' border=0 align=absmiddle> "
          else
            response.write "<a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show&edit=up" & cstr(i) & "'><img src='up.png' alt='Up' border=0 align=absmiddle></a> "
          end if
        if i = count then
            response.write "<img src='nothing.png' border=0 align=absmiddle> "
          else
            response.write "<a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show&edit=down" & cstr(i) & "'><img src='down.png' alt='Down' border=0 align=absmiddle></a> "
          end if
        response.write "<b>" & hs.StringItem(s,1,";") & "</b> "
        response.write "("
        DisplayTrickItemDetails(s)
        response.write ")</li>"
        next
      response.write "</ol>"
    end if
  ' show the bottom row of links/menu
  response.write "<table border=0 width='100%'><tr><td><select name='add' onchange='window.location.href=this.options[this.selectedIndex].value;'>"
  response.write "<option selected value='#'>Add:</option>"
  response.write "<option value='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=adddevice'>Device</option>"
  response.write "<option value='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=addevent'>Event</option>"
  response.write "<option value='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=addlink'>Link</option>"
  response.write "<option value='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=addframe'>Frame</option>"
  response.write "<option value='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=addsnippet'>Snippet</option>"
  response.write "<option value='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=addtrick'>Trick</option>"
  response.write "<option value='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=addloc'>Location</option>"
  response.write "<option value='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=addgrp'>Event Group</option>"
  response.write "<option value='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show&edit=clock'>Clock</option>"
  response.write "</select></td><td align=center>"
  response.write "<a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=rename'>Rename</a> &nbsp; "
  response.write "<a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=delete'>Delete</a></td>"
  response.write "<td align=right>"
  response.write "<a href='../../rover.asp?show=trick" & NameToWord(trick) & "'>Rover</a> &nbsp; "
  response.write "<a href='rover_tricks.asp?trick=none&action=tricklist'>Exit</a></td>"
  response.write "</td></tr></table>"
end sub

sub Screen_RenameTrick(trick)
  response.write "<p>Give your trick a new name:</p>"
  response.write "<form action='rover_tricks.asp' method=get><input type='text' name='name' value='" & trick & "'><input type='hidden' name='action' value='rename'><input type='hidden' name='trick' value='" & trick & "'><input type='submit' value='Rename'></form>"
  response.write "<p align=right><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>Exit</a></p>"
end sub

sub Screen_RenamedTrick(trick, name)
  dim tricknum
  tricknum = TrickNumber(trick)
  if tricknum = 0 then
    response.write "<p><font color=red>Something went wrong, I can't find the original trick.</font></p>"
  else
    hs.SaveINISetting "Tricks",cstr(tricknum),name,inifile
    response.write "<p>The trick named " & trick & " has been renamed to <a href='rover_tricks.asp?trick=" & NameToWord(name) & "&action=show'>" & name & "</a>.</p>"
  end if
  response.write "<p align=right><a href='rover_tricks.asp?trick=none&action=tricklist'>Exit</a></p>"
end sub

sub Screen_DeleteTrick(trick)
  response.write "<p>Do you really want to delete the trick <a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>" & trick & "</a>?<br>Note that this action cannot be undone.</p>"
  response.write "<p><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=deleted'>Confirm Delete</a></p>"
  response.write "<p align=right><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>Exit</a></p>"
end sub

sub Screen_DeletedTrick(trick)
  dim tricknum
  tricknum = TrickNumber(trick)
  if tricknum = 0 then
    response.write "<p><font color=red>Something went wrong, I can't find the original trick.</font></p>"
  else
    DeleteATrick tricknum
    response.write "<p>The trick named " & trick & " has been deleted.</p>"
  end if
  response.write "<p align=right><a href='rover_tricks.asp?trick=none&action=tricklist'>Exit</a></p>"
end sub

sub Screen_AddDevice(trick)
  dim dev, devices, devname, count
  dim locations(64), numlocations, i, j, k, l
  response.write "<p>Choose a device to add to trick <a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>" & trick & "</a>:</p>"

  ' loop through devices
  set devices = hs.GetDeviceEnumerator
  if IsObject(devices) then
      do while not devices.Finished
        set dev = devices.GetNext
        if not dev is nothing then
            l = 1 ' yes, add it
            ' see if this location is already listed
            for k = 1 to numlocations
              if lcase(dev.location) = lcase(locations(k)) then
                  l = 0 ' don't bother, it's already there
                  exit for
                end if
               next
            ' if we're due to add it to the list,
            if l = 1 and numlocations < 64 then ' add it
                numlocations = numlocations + 1
                locations(numlocations) = dev.location
              end if
          end if
        loop
    end if

  ' sort the list -- simple selection sort
  if numlocations > 2 then ' sort the list
      for i = 1 to numlocations-1
        k = i
        for j = i+1 to numlocations
          if locations(j) < locations(k) then k = j
          next
        if k <> i then
            s = locations(k)
            locations(k) = locations(i)
            locations(i) = s
          end if
        next
    end if    

  if numlocations = 0 then
      response.write "<p><font color=red>Something went wrong, I can't find any devices.</font></p>"
    else
      response.write "<ul>"
      for i = 1 to numlocations
        if locations(i) = "" then 
          response.write "<li>No Location</li><ul>"
        else
          response.write "<li>" & locations(i) & "</li><ul>"
        end if
        set devices = hs.GetDeviceEnumerator
        if IsObject(devices) then
          do while not devices.Finished
            set dev = devices.GetNext
            if not dev is nothing then
              if lcase(dev.location) = lcase(locations(i)) then 
                response.write "<li><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show&edit=dev" & dev.hc & dev.dc & "'>" & dev.name & "</a> (address " & dev.hc & dev.dc & ", type " & dev.dev_type_string & ")</li>"
                end if
              end if
            loop
          end if
        response.write "</ul>"
        next
      response.write "</ul>"
    end if
  response.write "<p align=right><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>Exit</a></p>"
end sub

sub Screen_AddEvent(trick)
  dim evt, events, i, j, k, s, evtname
  dim eventlist(256)
  dim numevents

  response.write "<p>Choose an event to add to trick <a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>" & trick & "</a>:</p>"
  count = 0

  set events = hs.GetEventEnumerator
  if IsObject(events) then
      do while not events.Finished
        set evt = events.GetNext
        if not evt is nothing then
            ' add to the events list
            if numevents < 256 then
                numevents = numevents + 1
                eventlist(numevents) = CStr(evt.name)
              end if
          end if
        loop
    end if

  ' sort the list -- simple selection sort
  if numevents > 2 then
      for i = 1 to numevents
        k = i
        for j = i+1 to numevents
          if eventlist(j) < eventlist(k) then k = j
          next
        if k <> i then
            s = eventlist(k)
            eventlist(k) = eventlist(i)
            eventlist(i) = s
          end if
        next
    end if

  if numevents = 0 then
    response.write "<p><font color=red>Something went wrong, I can't find any events.</font></p>"
  else
    response.write "<ul>"
    for i = 1 to numevents
      response.write "<li><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show&edit=evt" & EncodeString(eventlist(i)) & "'>" & eventlist(i) & "</a></li>"
      next
    response.write "</ul>"
  end if
  response.write "<p align=right><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>Exit</a></p>"
end sub

sub Screen_AddLink(trick, linktype)
  ' linktype 0=link, type 1=frame, type 2=snippet
  response.write "<p>Paste the URL of the web page you would like to add"
  if linktype = 1 then response.write " in a frame"
  if linktype = 2 then response.write " as a snippet"
  response.write " to <a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>" & trick & "</a>:</p>"
  response.write "<p><form action='rover_tricks.asp' method=get><input type='hidden' name='trick' value='" & trick & "'><input type='hidden' name='action' value='show'><input type='hidden' name='edit' value='"
  if linktype = 0 then 
      response.write "link"
    elseif linktype = 1 then
      response.write "frame"
    elseif linktype = 2 then
      response.write "snippet"
    end if
  response.write "'><input type='text' name='name' value=''><input type='submit' value='Add Link'></form></p>"
  response.write "<p align=right><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>Exit</a></p>"
end sub

sub Screen_AddTrick(trick)
  dim dev, devices, devname, count, i
  count = cint(hs.GetINISetting("Tricks","Count","0",inifile))
  if count <= 1 then
      response.write "<i>You have no tricks you can add to this one.</i><br>"
    else
      response.write "<p>Choose a trick to add to trick <a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>" & trick & "</a>:</p>"
      response.write "<ul>"
      for i = 1 to count
        s = hs.GetINISetting("Tricks",cstr(i),"",inifile)
        if s <> trick then
          response.write "<li><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show&edit=trick" & NameToWord(s) & "'>" & s & "</a>"
          response.write " (" & hs.GetINISetting("Trick" & cstr(i),"Count",0,inifile) & " items)</li>"
        end if
        next
      response.write "</ul>"
    end if
  response.write "<p align=right><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>Exit</a></p>"
end sub

sub Screen_AddLoc(trick)
  dim dev, devices, locations(64), numlocations, i, j, k, l
  numlocations = 0

  ' loop through devices
  set devices = hs.GetDeviceEnumerator
  if IsObject(devices) then
      do while not devices.Finished
        set dev = devices.GetNext
        if not dev is nothing then
            l = 1 ' yes, add it
            ' see if this location is already listed
            for k = 1 to numlocations
              if lcase(dev.location) = lcase(locations(k)) then
                  l = 0 ' don't bother, it's already there
                  exit for
                end if
               next
            ' if we're due to add it to the list,
            if l = 1 and numlocations < 64 then ' add it
                numlocations = numlocations + 1
                locations(numlocations) = dev.location
              end if
          end if
        loop
    end if

  ' sort the list -- simple selection sort
  if numlocations > 2 then ' sort the list
      for i = 1 to numlocations-1
        k = i
        for j = i+1 to numlocations
          if locations(j) < locations(k) then k = j
          next
        if k <> i then
            s = locations(k)
            locations(k) = locations(i)
            locations(i) = s
          end if
        next
    end if    
  response.write "<p>Choose a location to add to trick <a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>" & trick & "</a>:</p>"
  for i = 1 to numlocations
    response.write "<li><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show&edit=loc" & EncodeString(locations(i)) & "'>"
    response.write locations(i)
    response.write "</a></li>"
    next
  response.write "<li><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show&edit=locAll'>All</a></li>"
  response.write "</ul>"
  response.write "<p align=right><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>Exit</a></p>"
end sub

sub Screen_AddGroup(trick)
  dim evt, events, i, j, k, s, evtname
  dim grouplist(64)
  dim numgroups
  numgroups = 0

  set events = hs.GetEventEnumerator
  if IsObject(events) then
      do while not events.Finished
        set evt = events.GetNext
        if not evt is nothing then
            ' if this group's not in the group list, add it
            j = 0
            for i = 1 to numgroups
              if LCase(grouplist(i)) = LCase(evt.group) then j = 1
              next
            if j = 0 then
                numgroups = numgroups + 1
                grouplist(numgroups) = evt.group
              end if
            ' add to the events list
          end if
        loop
    end if

  ' sort the list -- simple selection sort
  if numgroups > 2 then
      for i = 1 to numgroups
        k = i
        for j = i+1 to numgroups
          if grouplist(j) < grouplist(k) then k = j
          next
        if k <> i then
            s = grouplist(k)
            grouplist(k) = grouplist(i)
            grouplist(i) = s
          end if
        next
    end if

  response.write "<p>Choose an event group to add to trick <a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>" & trick & "</a>:</p>"
  for i = 1 to numgroups
    response.write "<li><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show&edit=grp" & EncodeString(grouplist(i)) & "'>"
    response.write grouplist(i)
    response.write "</a></li>"
    next
  response.write "</ul>"
  response.write "<p align=right><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>Exit</a></p>"
end sub

sub Screen_RenameItem(trick, itemnumber)
  dim trickitem
  trickitem = hs.GetINISetting("Trick" & TrickNumber(trick),cstr(itemnumber),"",inifile)
  response.write "<p>Give your trick item a new name:</p>"
  response.write "<p><form action='rover_tricks.asp' method=get><input type='hidden' name='trick' value='" & trick & "'><input type='hidden' name='action' value='show'><input type='hidden' name='edit' value='name" & itemnumber & "'><input type='text' name='name' value='" & hs.StringItem(trickitem,1,";") & "'><input type='submit' value='Rename'></form></p>"
  response.write "<p>"
  DisplayTrickItemDetails(trickitem)
  response.write "</p>"
  response.write "<p align=right><a href='rover_tricks.asp?trick=" & NameToWord(trick) & "&action=show'>Exit</a></p>"
end sub

sub HandleEdits(trick, edit, name)
  dim itemnumber, itemvalue, dev, devname
  ' &edit=clock adds a clock to the end before showing (default name is "Clock")
  if LCase(edit) = "clock" then
    AddItemToTrick trick, "Clock", "clock", ""
    exit sub
  end if
  ' &edit=dev<devcode> adds a device to the end before showing (default name is device name)
  if LCase(left(edit,3)) = "dev" then
    itemvalue = mid(edit,4)
    set dev = hs.GetDeviceByRef(hs.DeviceExistsRef(itemvalue))
    devname = "Device " & itemvalue
    if not dev is nothing then devname = dev.name
    AddItemToTrick trick, devname, "dev", itemvalue
    exit sub
  end if
  ' &edit=evt<event> adds an event to the end before showing (default name is event name)
  if LCase(left(edit,3)) = "evt" then
    itemvalue = WordToName(mid(edit,4))
    AddItemToTrick trick, itemvalue, "evt", itemvalue
    exit sub
  end if
  ' &edit=link&name=<link> adds a link to the end before showing (default name is last part of link)
  ' &edit=frame&name=<link> adds a framed link to the end before showing (default name is last part of link)
  if LCase(edit) = "link" or LCase(edit) = "frame" or LCase(edit) = "snippet" then
    itemvalue = DecodeString(name)
    AddItemToTrick trick, FirstMeaningfulWordFromURL(itemvalue), LCase(edit), itemvalue
    exit sub
  end if
  ' &edit=trick<name> adds a trick to the end before showing (default name is trick name)
  if LCase(left(edit,5)) = "trick" then
    itemvalue = WordToName(mid(edit,6))
    AddItemToTrick trick, itemvalue, "trick", itemvalue
    exit sub
  end if
  ' &edit=loc<name> adds a location to the end before showing (default name is location name)
  if LCase(left(edit,3)) = "loc" then
    itemvalue = WordToName(DecodeString(mid(edit,4)))
    AddItemToTrick trick, itemvalue, "loc", itemvalue
    exit sub
  end if
  ' &edit=grp<name> adds an event group to the end before showing (default name is group name)
  if LCase(left(edit,3)) = "grp" then
    itemvalue = WordToName(DecodeString(mid(edit,4)))
    AddItemToTrick trick, itemvalue, "grp", itemvalue
    exit sub
  end if
  ' &edit=del<item> deletes the item before showing
  if LCase(left(edit,3)) = "del" then
    DeleteItemFromTrick trick, cint(mid(edit,4))
    exit sub
  end if
  ' &edit=up<item> moves the item up before showing
  if LCase(left(edit,2)) = "up" then
    SwapItemsInTrick trick, cint(mid(edit,3)-1)
    exit sub
  end if
  ' &edit=down<item> moves the item down before showing
  if LCase(left(edit,4)) = "down" then
    SwapItemsInTrick trick, cint(mid(edit,5))
    exit sub
  end if
  ' &edit=name<item>&name=<newname> renames the item before showing
  if LCase(left(edit,4)) = "name" then
    itemnumber = cint(mid(edit,5))
    trickitem = hs.GetINISetting("Trick" & TrickNumber(trick),cstr(itemnumber),"",inifile)
    trickitem = name & ";" & hs.StringItem(trickitem,2,";") & ";" & hs.StringItem(trickitem,3,";") 
    hs.SaveINISetting "Trick" & TrickNumber(trick),cstr(itemnumber),trickitem,inifile
    exit sub
  end if
end sub

' *****************************************************************************
function EncodeString(s)
  ' converts a string to URL-encoding (e.g., Frank%26s_Room)
  dim i, c, result
  result = ""
  for i = 1 to len(s)
    c = mid(s,i,1)
    if c = " " then
        result = result & "_"
      elseif c = "." or c = "-" then
        result = result & c
      elseif c < "0" or (c > "9" and c < "A") or (c > "Z" and c < "a") or (c > "z") then
        result = result & "%" & hex(asc(c))
      else
        result = result & c
      end if
    next
  EncodeString = result
end function

function DecodeString(s)
  ' reverses the above
  dim i, c, result
  result = ""
  for i = 1 to len(s)
    c = mid(s,i,1)
    if c = "%" then
        result = result & chr(mid(s,i+1,2))
        i = i + 2
      else
        result = result & c
      end if
    next
  DecodeString = result
end function

' *****************************************************************************
function GetUserAuthorizations(username)
  dim sUserList, sUsers, sUser, sUsername, sRights, i
  sUserList = hs.GetUsers
  sUsers = Split(sUserList,",")
  for i = 0 to UBound(sUsers)
    sUser = sUsers(i)
    sUsername = left(sUser,instr(sUser,"|")-1)
    sRights = cint(trim(mid(sUser,instr(sUser,"|")+1)))
    if sUsername = username then
        GetUserAuthorizations = sRights
        exit function
      end if
    next
  GetUserAuthorizations = 0
end function

' *****************************************************************************
sub hs_install()
  ' nothing to do
end sub

sub hs_uninstall()
	dim fso, fname
	set fso = CreateObject("Scripting.FileSystemObject")
  fname = hs.GetAppPath & "\Config\" & inifile
	if fso.FileExists(fname) then fso.DeleteFile fname
end sub

%>
